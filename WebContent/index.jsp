<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.util.*, java.io.*" %>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SWIM</title>
<link href="estilos/principal.css" rel="stylesheet" type="text/css" />
<!-- Attach our CSS -->
	  	<link rel="stylesheet" href="estilos/orbit-1.2.3.css">
	  	
		<!-- Attach necessary JS -->
		<script type="text/javascript" src="slider/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="slider/jquery.orbit-1.2.3.min.js"></script>	
		
			<!--[if IE]>
			     <style type="text/css">
			         .timer { display: none !important; }
			         div.caption { background:transparent; filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000,endColorstr=#99000000);zoom: 1; }
			    </style>
			<![endif]-->
		
		<!-- Run the plugin -->
		<script type="text/javascript">
			$(window).load(function() {
				$('#featured').orbit();
			});
		</script>
</head>
<body>
<div id="tudo">
	
	<div id="header">
		<jsp:include page="cabecalho.jsp"></jsp:include>
	</div>
	
	<div id="content">
		<div id="esquerda">
			<jsp:include page="menu.jsp"></jsp:include>
			
			<a href="index.jsp?pag=cadastrarFalePrefeito"><img src="banners/falePrefeita.jpg" border="0" style="margin-top: 10px;" /></a>
		</div>		
		
		<div id="direita">
			<%--- Choose para escolher a página apropriada --%>		
			<c:choose>
				
				<c:when test='${empty param.pag}'>
					<jsp:include page="principal.jsp"></jsp:include>
				</c:when >
				
				<c:when test='${not empty param.pag}'>
					<jsp:include page="${param.pag}.jsp"></jsp:include>
				</c:when >					
				
				<c:otherwise>
					<h1>Nenhuma página selecionada</h1>
				</c:otherwise>				
			</c:choose >
		</div>

	</div>
	
	<div id="footer">
		<jsp:include page="rodape.jsp"></jsp:include>
	</div>

</div>
</body>
</html>