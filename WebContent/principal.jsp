<%@ taglib prefix="myTags" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="principal">
	
	<div id="slider">
	
		<div class="contenter">
			<div id="featured"> 
				<div class="content" style="">
				</div>
		<jsp:useBean id="daoban" class="model.dao.DaoBanner" scope="request"></jsp:useBean>
		<c:forEach var="banner" items="${daoban.listBanHome}">
			<a href="http://${banner.ban_contato_site}" target="_blank"><img src="admin/exibirBanner.do?id=${banner.ban_id}" width="758" height="255" style="float: left;"/></a>
		</c:forEach>
			</div>
			
		</div>
		
	</div>
	
	
	
	<div id="noticias">	
		<jsp:useBean id="dao" class="model.dao.DaoNoticia" scope="request"></jsp:useBean>
		<c:forEach var="noticia" items="${dao.listNoticiasHome}">
			<div class="NoticiasBoxHome">
			<a href="noticiaExibe.do?not_id=${noticia.not_id}"><img src="admin/imagemNoticia.do?id=${noticia.not_id}" width="70" height="70" style="float: left; margin-right: 5px;"/>
			<p class="notTitulo">${noticia.not_titulo}</p>
			<p>${noticia.not_descricao}</p></a>
			</div>
		</c:forEach>	
	</div>
	
	<div id="intinerario">
		<a href="index.jsp?pag=transportes"><img src="banners/bannerOnibus.jpg" border="0" /></a>
	</div>
	
	<div id="eventos"><myTags:eventos></myTags:eventos></div>
	
	<div id="banners">
		<a href="http://www.transparencia.org.br/pindoretama"><img src="banners/banner_menor_1.jpg" width="175" height="75" border="0" /></a>
		<a href="http://www.ceara.ce.gov.br/diariooficial"><img src="banners/banner_menor_2.jpg" width="175" height="75" border="0" /></a>
		<a href="http://www.ceara.ce.gov.br"><img src="banners/banner_menor_3.jpg" width="175" height="74" border="0" /></a>
	</div>	

</div>