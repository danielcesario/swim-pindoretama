<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

<h1>Eventos</h1>

<jsp:useBean id="eventos" class="model.dao.EventosDao" scope="page"/>
<c:forEach var="evento" items="${eventos.consulta}">
	<c:if test="${evento.idEvento eq param.id}">
		<h2>${evento.titulo}</h2>
		<p><img src="exibirEvento.do?id=${param.id}" style="float:right; margin-left: 10px;" width="200" >
		${evento.descricao}
		</p>
	</c:if>
</c:forEach>  	
