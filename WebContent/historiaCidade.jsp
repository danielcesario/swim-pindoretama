<h1>Hist�ria da Cidade</h1>

<h2>Origem da cidade</h2>
<p>Pindoretama, como a grande parte dos munic�pios brasileiros, surgiu a margem de uma estrada. 
Corria os anos de 1876,77. O governo imperial de D. Pedro II decidiu fazer a liga��o telef�nica 
entre Aracati e Fortaleza. Foi necess�rio fazer uma estrada para passar a linha telegrafa, isto �, 
os postes com a fia��o. No dia 17 de Fevereiro de 1878, foi inaugurada a linha telef�nica entre 
Aracati e Fortaleza. A estrada tinha uma extens�o de 141,276Km. esta estrada chamou-se a principio 
estrada nova, estrada telegr�fica ou estrada do fio. Passava no centro de Pindoretama. 
� a atual Avenida capit�o Nogueira. Conclu�da a estrada, algumas pessoas vieram construir 
suas casas, � margem dela. Assim, depois de 10 anos, Pindoretama j� era um pequeno povoado.</p>

<h2>Informa��es gerais</h2>
<p>ddd(85)<br />
cep: 62860-000<br />
Dist�ncia de Fortaleza: 49,3 km<br />
Tempo estimado de viagem: 42 min<br />
Vias de acesso: CE-040<br />
Altitude: 40m<br />
Popula��o: 18.322 habitantes(IBGE 2009)<br />
Temperatura: MAX: 31� C - MIN: 26� C</p>

<h2>Geografia do munic�pio</h2>
<p>�rea: 72,855 km�<br />
Latitude: 4� 01' 37<br />
Longitude: 38� 18' 18<br />
Clima: Tropical quente semi-�rido brando e Tropical quente sub-�mido com chuvas de janeiro a maio<br />
Relevo: Tabuleiros pr�-litor�neos dissecados em interfl�vios tabulares<br />
Vegeta��o: Complexo vegetacional da zona litor�nea<br />
Precipita��o pluviom�trica: 930,7 mm (m�dia hist�rica)<br />
Recursos h�dricos ( 2007): 23 po�os</p>