<h1>Pontos Tur�sticos</h1>
<h2>Igreja Matriz de Pindoretama</h2> 
<p><img src="imagens/cidade/turismo_igrejamatriz.jpg" class="imagens" />A Pra�a da Matriz de Pindoretama, situada no centro da cidade �s margens da Av. Cap. Nogueira, � um dos principais pontos de encontros dos pindoretamenses.
Um lugar aconchegante onde crian�as brincam com suas fam�lias em seguran�a. Diariamente se v�em senhores e senhoras conversando sentados nos bancos de madeira sob as sombras dos seus p�s de jambo.
Na Pra�a da Matriz passam todos os dias centenas, milhares de pessoas, dentre elas alunos, universit�rios e professores de diversas escolas p�blicas e particulares que ali pegam seus transportes rumo as suas escolas.
L� s�o realizadas algumas das maiores festas religiosas do munic�pio, tais como a da Padroeira e das Santas Miss�es. � na Pra�a da Matriz que os desfilantes do "7 de setembro" passam em contin�ncia as autoridades que ficam nos palanques bem ao lado da Pra�a.
A noite pode-se degustar em modestas vendas uma tapioca caseira, bolo de fub�, ch� de capim-santo, dentre outros aperitivos, de maneira bem simples e artesanal, al�m de churrasquinhos e sorvetes.
</p>

<h2>Engenhos de cana-de-a��car</h2> 
<p><img src="imagens/cidade/turismo_canaacucar.jpg" class="imagens" />Os engenhos de cana-de-a��car j� fazem parte do cart�o postal da cidade. Nos �ltimos anos, eles v�m se tornando pontos tur�sticos do munic�pio, visitados pela maioria dos turistas que viajam pelo litoral leste.</p>

<p style="clear: both;">&nbsp;</p>

<h2 style="clear: both">Feira do Mercado P�blico</h2> 
<p><p><img src="imagens/cidade/turismo_mercado.jpg" class="imagens" />A Feira do mercado p�blico, assim como � conhecida, tr�s consigo muitos anos de exist�ncia, feirantes de v�rios lugares trazem seus produtos para serem comercializados.
Podemos encontrar diversos tipos de produtos, do cal�ado � bolsa, encontramos tamb�m artesanato, como: bordados, pe�as de barro, etc.</p> 

<p style="clear: both">&nbsp;</p>