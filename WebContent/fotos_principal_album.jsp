<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
	1 ) Reference to the files containing the JavaScript and CSS.
	These files must be located on your server.
-->

<script type="text/javascript" src="highslide/highslide.js"></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />


<!--
	2) Optionally override the settings defined at the top
	of the highslide.js file. The parameter hs.graphicsDir is important!
-->

<script type="text/javascript">
	hs.graphicsDir = 'highslide/graphics/';
	hs.wrapperClassName = 'wide-border';
</script>



<div id="user_content">
	<c:set var="listaFoto" value="${requestScope.fotos}" />
	<h1>
		Voc� est� no �lbum
		<c:out value="${listaFoto[0].album}" />
	</h1>
	<br>

	<table class="tabListar">
		<c:forEach var="ft" items="${listaFoto}">
			<tr>

				<td width="100" valign="top">


					<div>
						<!--
	3) This is how you mark up the thumbnail image with an anchor tag around it.
	The anchor's href attribute defines the URL of the full-size image.
-->
						<a href="exibirFoto.do?id=${ft.idFoto}" class="highslide"
							onclick="return hs.expand(this)"><img border="0"
							src="exibirFoto.do?id=${ft.idFoto}" width="100"
							alt="Highslide JS" title="Click to enlarge">
						</a>

					</div></td>

				<td valign="top">
					<h1 style="margin: 0px">
						<b>${ft.titulo}</b>
					</h1> <b>Descri��o: </b>${ft.descricao}</td>

			</tr>
		</c:forEach>
	</table>
	<br>



</div>
