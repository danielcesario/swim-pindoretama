<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Resultados</h1>

<h2>${msgAlerta}</h2>

<p>&nbsp;</p>

<c:forEach var="noticia" items="${noticiasBusca}">
	<div class="NoticiasBoxAll">
	<a href="noticiaExibe.do?not_id=${noticia.not_id}"><img src="admin/imagemNoticia.do?id=${noticia.not_id}" width="70" height="70" style="float: left; margin-right: 5px;"/>
	<p class="notTitulo">${noticia.not_titulo}</p>
	<p>${noticia.dataHoraFormatada}</p>
	<p>${noticia.not_descricao}</p></a>
	</div>
</c:forEach>
