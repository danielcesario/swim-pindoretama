<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<c:if test="${not empty msgAlerta}">
	<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;line-height: 25px;"><b>${msgAlerta}</b></p>
</c:if>

<c:if test="${param.enviado ne 1 }">
<c:set var="prefeito" value="${requestScope.prefeito}" scope="page"/>
	<h1>Fale com o Prefeito</h1>
	
	<p>&nbsp;</p>
	
	<form action='falePrefeito.do' method="post">
	<table width="700" border="0" style="color: orange; font-weight: bold; margin-left: 10px;">
	
	<tr>
	<td width="100"><label class='fpf_campo'> *Nome: </td>
	<td><input type="text" name='nome' size='40' value="${prefeito.remetente}"></label></td>
	</tr>
	
	<tr>
	<td><label class='fpf_campo'> Telefone:</td>
	<td><input type="text" name='telefone' size='20' value="${prefeito.telefone}"></label></td>
	<tr>
	
	<tr>
	<td><label class='fpf_campo'> E-mail:</td>
	<td><input type="text" name='email' size='30' value="${prefeito.email}"></label></td>
	</tr>
	
	<tr>
	<td valign="top"><label class='fpf_campo'> *Descrição:</td>
	<td><TEXTAREA rows=15 cols=40 name="descricao" value="${prefeito.descricao}"></TEXTAREA></label></td>
	</tr>
	
	</table>
	
	<input type="submit" value="Enviar">
	
	</form>
	
	<p style="font-size: 12px">*Campos de preenchimento obrigatório</p>
</c:if>

<c:if test="${param.enviado eq 1 }">
     Mensagem enviada com Sucesso.
</c:if>