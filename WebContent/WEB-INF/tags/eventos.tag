<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:useBean id="eventos" scope="request" class="model.dao.EventosDao"/>
<h2>Eventos</h2>
<c:forEach var="evento" items="${eventos.consultaLimite}">
	<a href="index.jsp?pag=eventosVisCompleta&id=${evento.idEvento}"><f:formatDate value="${evento.data}" pattern="dd/MM/yyyy"/> - ${evento.titulo}</a><br>
</c:forEach>