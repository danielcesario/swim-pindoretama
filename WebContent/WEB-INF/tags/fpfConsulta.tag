<%@ tag pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@attribute name="filtro" %>
<%@attribute name="campo" rtexprvalue="true"%> <!-- rtexprvalue(run time expression value) para a tag aceitar runtime --> 


<jsp:useBean id="falePrefeito" scope="request" class="model.dao.FalePrefeitoDao"/>


<c:if test="${empty campo}">
<c:forEach var="entradas" items="${falePrefeito.getList()}">
	<p> <label class="fpf_con"><b>Data:</b> <f:formatDate value="${entradas.data}" pattern="dd/MM/yyyy"/> </label></p>
	<p> <label class="fpf_con"><b>Remetente:</b> ${entradas.remetente} </label></p>
	<p> <label class="fpf_con"><b>Telefone:</b> ${entradas.telefone} </label>
   	<label class="fpf_con"><b>E-mail:</b> ${entradas.email}</label> </p>
	<p> <label class="fpf_con"><b>Descrição:</b> ${entradas.descricao}</label> </p>	
	<p> <label class="fpf_con"><b>Estado:</b> <c:if test="${entradas.estado eq 'e'}">Enviado</c:if><c:if test="${entradas.estado eq 'p'}">Pendente</c:if></label> 
	<label class="fpf_con"><b>Setor:</b><c:if test="${entradas.setor eq 'indefinido'}">Indefinido</c:if><c:if test="${entradas.setor ne 'indefinido'}">Secretaria de ${entradas.setor}</c:if></label></p>
	<hr>	
</c:forEach>	
</c:if>

<c:if test="${filtro eq 'data'}">	
	<c:forEach var="entradas" items="${falePrefeito.getList()}">	
	<f:formatDate var= "dataTemp" value = "${entradas.data}" pattern="dd/MM/yyyy"/>			
		<c:if test="${dataTemp eq campo}">
			<p> <label class="fpf_con"><b>Data:</b> <f:formatDate value="${entradas.data}" pattern="dd/MM/yyyy"/> </label></p>
			<p> <label class="fpf_con"><b>Remetente:</b> ${entradas.remetente} </label></p>
			<p> <label class="fpf_con"><b>Telefone:</b> ${entradas.telefone} </label>
   			<label class="fpf_con"><b>E-mail:</b> ${entradas.email}</label> </p>
			<p> <label class="fpf_con"><b>Descrição:</b> ${entradas.descricao}</label> </p>	
			<p> <label class="fpf_con"><b>Estado:</b> <c:if test="${entradas.estado eq 'e'}">Enviado</c:if><c:if test="${entradas.estado eq 'p'}">Pendente</c:if></label> 
			<label class="fpf_con"><b>Setor:</b><c:if test="${entradas.setor eq 'indefinido'}">Indefinido</c:if><c:if test="${entradas.setor ne 'indefinido'}">Secretaria de ${entradas.setor}</c:if></label></p>
			<hr>	
		</c:if>
	</c:forEach> 
</c:if>

<c:if test="${filtro eq 'remetente'}">
	<c:forEach var="entradas" items="${falePrefeito.getList()}">
		<c:if test="${entradas.remetente eq campo}">
			<p> <label class="fpf_con"><b>Data:</b> <f:formatDate value="${entradas.data}" pattern="dd/MM/yyyy"/> </label></p>
			<p> <label class="fpf_con"><b>Remetente:</b> ${entradas.remetente} </label></p>
			<p> <label class="fpf_con"><b>Telefone:</b> ${entradas.telefone} </label>
   			<label class="fpf_con"><b>E-mail:</b> ${entradas.email}</label> </p>
			<p> <label class="fpf_con"><b>Descrição:</b> ${entradas.descricao}</label> </p>	
			<p> <label class="fpf_con"><b>Estado:</b> <c:if test="${entradas.estado eq 'e'}">Enviado</c:if><c:if test="${entradas.estado eq 'p'}">Pendente</c:if></label> 
			<label class="fpf_con"><b>Setor:</b><c:if test="${entradas.setor eq 'indefinido'}">Indefinido</c:if><c:if test="${entradas.setor ne 'indefinido'}">Secretaria de ${entradas.setor}</c:if></label></p>
			<hr>	
		</c:if>
	</c:forEach>
</c:if>

<c:if test="${filtro eq 'email'}">
	<c:forEach var="entradas" items="${falePrefeito.getList()}">
		<c:if test="${entradas.email eq campo}">
			<p> <label class="fpf_con"><b>Data:</b> <f:formatDate value="${entradas.data}" pattern="dd/MM/yyyy"/> </label></p>
			<p> <label class="fpf_con"><b>Remetente:</b> ${entradas.remetente} </label></p>
			<p> <label class="fpf_con"><b>Telefone:</b> ${entradas.telefone} </label>
   			<label class="fpf_con"><b>E-mail:</b> ${entradas.email}</label> </p>
			<p> <label class="fpf_con"><b>Descrição:</b> ${entradas.descricao}</label> </p>	
			<p> <label class="fpf_con"><b>Estado:</b> <c:if test="${entradas.estado eq 'e'}">Enviado</c:if><c:if test="${entradas.estado eq 'p'}">Pendente</c:if></label> 
			<label class="fpf_con"><b>Setor:</b><c:if test="${entradas.setor eq 'indefinido'}">Indefinido</c:if><c:if test="${entradas.setor ne 'indefinido'}">Secretaria de ${entradas.setor}</c:if></label></p>
			<hr>	
		</c:if>
	</c:forEach>
</c:if>

<c:if test="${filtro eq 'estado'}">
	<c:forEach var="entradas" items="${falePrefeito.getList()}">
		<c:if test="${entradas.estado eq campo}">
			<p> <label class="fpf_con"><b>Data:</b> <f:formatDate value="${entradas.data}" pattern="dd/MM/yyyy"/> </label></p>
			<p> <label class="fpf_con"><b>Remetente:</b> ${entradas.remetente} </label></p>
			<p> <label class="fpf_con"><b>Telefone:</b> ${entradas.telefone} </label>
   			<label class="fpf_con"><b>E-mail:</b> ${entradas.email}</label> </p>
			<p> <label class="fpf_con"><b>Descrição:</b> ${entradas.descricao}</label> </p>	
			<p> <label class="fpf_con"><b>Estado:</b> <c:if test="${entradas.estado eq 'e'}">Enviado</c:if><c:if test="${entradas.estado eq 'p'}">Pendente</c:if></label> 
			<label class="fpf_con"><b>Setor:</b><c:if test="${entradas.setor eq 'indefinido'}">Indefinido</c:if><c:if test="${entradas.setor ne 'indefinido'}">Secretaria de ${entradas.setor}</c:if></label></p>
			<hr>	
		</c:if>
	</c:forEach>
</c:if>

<c:if test="${filtro eq 'setor'}">
	<c:forEach var="entradas" items="${falePrefeito.getList()}">
		<c:if test="${entradas.setor eq campo}">
			<p> <label class="fpf_con"><b>Data:</b> <f:formatDate value="${entradas.data}" pattern="dd/MM/yyyy"/> </label></p>
			<p> <label class="fpf_con"><b>Remetente:</b> ${entradas.remetente} </label></p>
			<p> <label class="fpf_con"><b>Telefone:</b> ${entradas.telefone} </label>
   			<label class="fpf_con"><b>E-mail:</b> ${entradas.email}</label> </p>
			<p> <label class="fpf_con"><b>Descrição:</b> ${entradas.descricao}</label> </p>	
			<p> <label class="fpf_con"><b>Estado:</b> <c:if test="${entradas.estado eq 'e'}">Enviado</c:if><c:if test="${entradas.estado eq 'p'}">Pendente</c:if></label> 
			<label class="fpf_con"><b>Setor:</b><c:if test="${entradas.setor eq 'indefinido'}">Indefinido</c:if><c:if test="${entradas.setor ne 'indefinido'}">Secretaria de ${entradas.setor}</c:if></label></p>
			<hr>	
		</c:if>
	</c:forEach>
</c:if>