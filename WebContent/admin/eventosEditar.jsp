<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="eventoTemp" value="${requestScope.evento}" scope="page"/>

<c:if test="${not empty msgAlerta}">
	<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;line-height: 25px;"><b>${msgAlerta}</b></p>
</c:if>

	<jsp:useBean id="eventos" class="model.dao.EventosDao" scope="request"></jsp:useBean>
	<c:forEach var="evento" items="${eventos.consulta}">
		<c:if test="${evento.idEvento eq param.id}">
			<c:set var="eventoTemp" value="${evento}" scope="request"/>			
		</c:if>
	</c:forEach>
	<h1> Eventos </h1>
	<h2>- Editar -</h2>
	
	<c:if test="${param.controle ne 1}">
		<form action="atualizarEvento.do" method="post" enctype="multipart/form-data">
			<p><label class='evt_campo'>T�tulo: <input type="text" name='titulo' size='40' value="${eventoTemp.titulo}"></label></p>
			<p><label class='evt_campo'>Data: <input type="text" name='data' size='20' value="<c:if test='${not empty eventoTemp.data}'><f:formatDate value="${eventoTemp.data}" pattern="dd/MM/yyyy"/></c:if>"></label></p>
			<p><label class='evt_campo'>Imagem:<img src="../exibirEvento.do?id=${eventoTemp.idEvento}" width="80" height="80" alt="divulgacao"><br><a href="index.jsp?pag=eventosEditar&id=${param.id}&controle=1">Alterar Foto</a></label></p>
			<p><label class='evt_campo'>Descri��o: <TEXTAREA rows=15 cols=40 name="descricao">${eventoTemp.descricao}</TEXTAREA><br></label>
			<p><input type="hidden" name="idUser" value="${sessionScope.usr_id}">  
			<p><input type="hidden" name="idEvt" value="${param.id}">  		
			<a href="" ><img alt="" src="" ><br> Consultar Eventos </a></p>
			<input type="submit" value="Gravar">
		</form>	
	</c:if>			
	                                                                
	<c:if test="${param.controle eq 1}">
		<form action="atualizarFotoEvento.do" method="post" enctype="multipart/form-data">			
			<p><label class='evt_campo'>T�tulo: <input type="text" name='titulo' size='40' value="${eventoTemp.titulo}"></label></p>
			<p><label class='evt_campo'>Data: <input type="text" name='data' size='20' value="<c:if test="${not empty eventoTemp.data}"> <f:formatDate value="${eventoTemp.data}" pattern="dd/MM/yyyy"/></c:if>"></p></label>
			<p><label class='evt_campo'>Imagem:<input type="file" name='imagem'></label></p>
			<p><label class='evt_campo'>Descri��o: <TEXTAREA rows=15 cols=40 name="descricao">${eventoTemp.descricao}</TEXTAREA><br></label>
			<p><input type="hidden" name="idUser" value="${sessionScope.usr_id}"> 
			<p><input type="hidden" name="idEvt" value="${param.id}">  			
			<a href="" ><img alt="" src="" ><br> Consultar Eventos </a></p>			
			<input type="submit" value="Gravar">						
		</form>	
	</c:if>
