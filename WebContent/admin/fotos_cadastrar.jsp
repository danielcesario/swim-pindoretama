<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:useBean id="dadosFoto" class="model.Foto" scope="request"></jsp:useBean>


<div id="admin_content">
	<h1> Cadastre uma foto</h1>
	
	<c:if test="${not empty msgAlerta}">
		<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;"><b>${msgAlerta}</b></p>
	</c:if>
	
	<br>
	<p class="FormCampo">Os campos com * s�o obrigat�rios.</p>
	
	
	
	
	<form action="cadastrarFoto.do" method="post" enctype="multipart/form-data">
		<p class="FormCampo"><label>T�tulo:<br /><input type="text" name='titulo' size='40' value="${dadosFoto.titulo}"></label></p>
		
		<jsp:useBean id="fotos" class="model.dao.FotosDao" scope="page"/>
		
		
		
		
		
		<p><label><input type="radio" name="option" value="1" checked/><font face="Tahoma" size="3" color="#e77600"><b>Selecione se quiser um �lbum existente:</b></font></label></p><br>
		
		<p><select name="album1">
		<option value="0">Selecione um �lbum</option>
 			<c:forEach var="ft" items="${fotos.consultaAlbum}">
				<option value="${ft}">${ft}</option>
			</c:forEach>
		</select></p><br>

    	<p><label><input type="radio" name="option" value="2"/><font face="Tahoma" size="3" color="#e77600"><b>Selecione se quiser um novo �lbum, insira o nome abaixo: </b></font></label></p><br>
		
		<p class="FormCampo"><input type="text" size='40' name="album2" ></p>

		<p class="FormCampo"><label>Imagem:<br /><input type="file" name='imagem'></label></p>
		<p class="FormCampo"><label>Descri��o: <br><TEXTAREA rows=5 cols=40 name="descricao">${dadosFoto.descricao}</TEXTAREA><br></label></p>
		<p class="FormCampo"><input type="hidden" name="idUser" value="${sessionScope.usr_id}">  <!-- ${sessionScope.usr_id} --></p>
		<p><input type="submit" value="Gravar"  class="FormBotao" ></p><br>
	</form>
</div>