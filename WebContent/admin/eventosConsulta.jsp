<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<h1>Eventos</h1>
<h2>- Consulta -</h2>
<a href="index.jsp?pag=eventosPrincipal"><img alt="" src="" align="right">Administra��o</a>
<h4>Lista de Eventos <hr></body></h4>	
<jsp:useBean id="eventos" scope="page" class="model.dao.EventosDao"/>
<table class="tabListar">
	<tr>
		<th style="width: 750px;">Eventos</th>
		<th style="width: 150px;">Op��es</th>
	</tr>
	<c:forEach var="evento" items="${eventos.consulta}">
		<tr>
			<td><b>Data:</b><f:formatDate pattern="dd/MM/yyyy" value="${evento.data}"/><br/>
				<b>Evento:</b> ${evento.titulo} 
			</td>
			<td style="text-align: center;">
			  <a href="index.jsp?pag=eventosEditar&id=${evento.idEvento}"><img src="../imagens/painel/editar.png" alt="editar" title="Editar"></a>&nbsp;&nbsp;&nbsp;
			  <a href="excluirEvento.do?id=${evento.idEvento}"><img src="../imagens/painel/delete.png" alt="deletar" title="Excluir"></a>			 
			</td>
		</tr>	 	
	</c:forEach>	
	</table> 
	<p>&nbsp;</p>