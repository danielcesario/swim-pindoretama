<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Controle de Usuários</h1>

<h2>Adicionar usuário</h2>

<c:if test="${not empty msgAlerta}">
	<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;"><b>${msgAlerta}</b></p>
</c:if>

<p>&nbsp;</p>

<jsp:useBean id="dadosUsuario" class="model.Usuario" scope="request"></jsp:useBean>

<form action="cadastrarUsuario.do" method="post">
	
	<p class="FormCampo"><label>*Nome Completo:<br />
	<input type="text" name="usr_nome" value="${dadosUsuario.usr_nome}" /></label></p>
	
	<p class="FormCampo"><label>*E-mail:<br />
	<input type="text" name="usr_email" value="${dadosUsuario.usr_email}" /></label></p>
	
	<p class="FormCampo"><label>Telefone:<br />
	<input type="text" name="usr_telefone" /></label></p>
	
	<p class="FormCampo"><label>*Login:<br />
	<input type="text" name="usr_login" value="${dadosUsuario.usr_login}" /></label></p>
	
	<p class="FormCampo"><label>*Senha:<br />
	<input type="text" name="usr_senha" value="${dadosUsuario.usr_senha}" /></label></p>
	
	<p><input type="submit" value="Cadastrar Usuário" class="FormBotao" /></p>

</form>
<p>&nbsp;</p>
<p>*Preenchimento obrigatório</p>
<p>&nbsp;</p>