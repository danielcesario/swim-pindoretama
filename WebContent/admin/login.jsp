<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Login Principal</h1>

<c:if test="${not empty msgAlerta}">
	<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;"><b>${msgAlerta}</b></p>
</c:if>

<p>&nbsp;</p>

<form action="login.do" method="post" id="formLogin">

	<p class="FormCampo"><label>Usu�rio<br />
	<input type="text" name="usuario" style="width: 350px;" /></label></p>
	
	<p class="FormCampo"><label>Senha<br />
	<input type="password" name="senha" style="width: 350px;" /></label></p>
	
	<p><input type="submit" value="Entrar" class="FormBotao" /></p>

</form>

<p>&nbsp;</p>