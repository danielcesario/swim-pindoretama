<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="admin_content">
<h1>Lista das empresas cadastradas</h1><br>
<h2><a href="index.jsp?pag=utilidadecadastro"><img border="0" src="../imagens/painel/adicionar.png" /> Adicionar Utilidade P�blica</a></h2><br>

<p>&nbsp;</p>

<table class="tabListar">
 
<jsp:useBean id="dao" class="model.dao.UtilidadeDao" scope="request"></jsp:useBean>
<c:forEach var="utilidade" items="${dao.listUtilidade}">
	<tr>
		<td width="100" valign="top">
		<img width="100" src="exibirLogo.do?id=${utilidade.id}">
		</td>
		
		<td class="utilidade_listar"><h1>${utilidade.nomeEmpresa}</h1>
		<p><b>Endere�o: </b>${utilidade.endereco}<br></p>
		<p><b>Telefone: </b>${utilidade.telefone}<br></p>
		<p><b>P�gina na web: </b><a href="${utilidade.url}">${utilidade.url}</a></p>
		</td>
		<td style="text-align: center;">
		<a href="exibirUtilidade.do?up_id=${utilidade.id}"><img src="../imagens/painel/editar.png" /></a>
		<a href="excluirUtilidade.do?up_id=${utilidade.id}"><img src="../imagens/painel/delete.png" /></a>
		</td>
	</tr>
</c:forEach>
		
</table>

<p>&nbsp;</p>
</div>