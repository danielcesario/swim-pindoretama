<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div id="admin_content">

<h1>Gerenciador de fotos</h1><br>
<h2><a href="index.jsp?pag=fotos_cadastrar"><img border="0" src="../imagens/painel/adicionar.png" /> Adicionar Foto</a></h2><br>

<jsp:useBean id="fotos" class="model.dao.FotosDao" scope="page"/>


<table class="tabListar">
	<c:forEach var="ft" items="${fotos.consultaAlbum}">
		<tr>
   			<td valign="top" class="linksAlbuns">
   				<a href="exibirFotoAlbum.do?foto_album=${ft}">${ft}</a>
   			</td>

  		</tr>
	</c:forEach>
</table><br>


</div>
