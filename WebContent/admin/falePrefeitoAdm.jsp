<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

	<h1>Fale com o Prefeito</h1>
	<p>&nbsp;</p>
	<h2>Entradas pendentes</h2>
	<hr>
	
	<jsp:useBean id="entradas" scope="page" class="model.dao.FalePrefeitoDao"/>
	<c:forEach var="entrada" items="${entradas.listPendentes}">
		<p><label class="fpf_adm">Data: <f:formatDate pattern="dd/MM/yyyy" value="${entrada.data}"/></label><br>
		<label class="fpf_adm">Remetente: ${entrada.remetente} </label><br>
		<label class="fpf_adm">Telefone: ${entrada.telefone} </label><br>
	    <label class="fpf_adm">E-mail: ${entrada.email}</label><br>
	    <label class="fpf_adm">Descri��o: ${entrada.descricao}</label> </p>	
		<form method="post" action="enviarEmail.do">
			<p><select name="setor" >
				<option value="Secretaria de Saude"> Secretaria de Saude </option>
				<option value="Secretaria de Educacao"> Secretaria de Educacao </option>
				<option value="Secretaria de Transportes"> Secretaria de Transportes </option>
				<option value="Secretaria de Seguranca"> Secretaria de Seguranca </option>
				<option value="Prefeito"> Prefeito  </option>							
			</select>
			<input type="hidden" name="id" value="${entrada.idPrefeito}">
			<input type="hidden" name="usr_id" value="${sessionScope.usr_id}"> 			
			<input type="image" src="../imagens/painel/encaminhar.png" title="Encaminhar" alt="Encaminhar Mensagem">					
			<a href="excluirFalePrefeito.do?id=${entrada.idPrefeito}"><img src="../imagens/painel/delete.png" /></a></p>
		</form>
		<hr>	
	</c:forEach>		