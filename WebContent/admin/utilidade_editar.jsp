<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Controle de Utilidade P�blica</h1><br>

<jsp:useBean id="dadosUtilidade" class="model.Utilidade" scope="request"></jsp:useBean>

<h2>Editar Utilidade P�blica</h2>

<c:if test="${not empty msgAlerta}">
	<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;"><b>${msgAlerta}</b></p>
</c:if>

<br>
<p class="FormCampo">Os campos com * s�o obrigat�rios:</p>

<form action="editarUtilidade.do" method="post" enctype="multipart/form-data">

	<input type="hidden" name="up_id" value="${utilidade.id}" />
	
	<p class="FormCampo"><label>Nome da Empresa*:<br />
	<input type="text" name="up_nomeEmpresa" value="${utilidade.nomeEmpresa}" /></label></p>
	
	<p class="FormCampo"><label>Endere�o*:<br />
	<input type="text" name="up_endereco" value="${utilidade.endereco}" /></label></p>
	
	<p class="FormCampo"><label>Telefone:<br />
	<input type="text" name="up_telefone" value="${utilidade.telefone}" /></label></p>
	
	<p class="FormCampo"><label>Logotipo:<br />
	<input type="file" name="logotipo" /></label></p>
	
	<p class="FormCampo"><label>P�gina na Web:<br />
	<input type="text" name="up_url" value="${utilidade.url}"/></label></p>	
	
	<p class="FormCampo"><input type="hidden" name="usr_id" value="${sessionScope.usr_id}"></p>
	
	
	<p><input type="submit" value="Atualizar" class="FormBotao" /></p>

</form>


<ckeditor:replace  replace="not_texto" basePath="../ckeditor/" />
<p>&nbsp;</p>