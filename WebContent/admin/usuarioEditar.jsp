<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Controle de Usuários</h1>

<h2>Editar usuário</h2>

<jsp:useBean id="usuario" class="model.Usuario" scope="request"></jsp:useBean>

<c:if test="${not empty msgAlerta}">
	<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;"><b>${msgAlerta}</b></p>
</c:if>

<p>&nbsp;</p>

<form action="atualizarUsuario.do" method="post">

	<input type="hidden" name="usr_id" value="${usuario.usr_id}" />
	
	<p class="FormCampo"><label>*Nome Completo:<br />
	<input type="text" name="usr_nome" value="${usuario.usr_nome}" /></label></p>
	
	<p class="FormCampo"><label>*E-mail:<br />
	<input type="text" name="usr_email" value="${usuario.usr_email}" /></label></p>
	
	<p class="FormCampo"><label>Telefone:<br />
	<input type="text" name="usr_telefone" value="${usuario.usr_telefone}" /></label></p>
	
	<p class="FormCampo"><label>*Login:<br />
	<input type="text" name="usr_login" value="${usuario.usr_login}" /></label></p>
	
	<p class="FormCampo"><label>*Senha:<br />
	<input type="text" name="usr_senha" value="${usuario.usr_senha}" /></label></p>
	
	<p><input type="submit" value="Atualizar Usuário" class="FormBotao" /></p>

</form>
<p>&nbsp;</p>
<p>*Preenchimento obrigatório</p>
<p>&nbsp;</p>