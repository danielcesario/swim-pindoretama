<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Controle de Not�cias</h1>

<h2>Editar not�cia</h2>

<c:if test="${not empty msgAlerta}">
	<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;"><b>${msgAlerta}</b></p>
</c:if>

<p>&nbsp;</p>

<jsp:useBean id="noticia" class="model.Noticia" scope="request"></jsp:useBean>

<form action="atualizarNoticia.do" method="post" enctype="multipart/form-data">

	<input type="hidden" name="not_id" value="${noticia.not_id}" />
	
	<p class="FormCampo"><label>*T�tulo:<br />
	<input type="text" name="not_titulo" value="${noticia.not_titulo}" /></label></p>
	
	<p class="FormCampo"><label>*Descri��o:<br />
	<input type="text" name="not_descricao" value="${noticia.not_descricao}" /></label></p>
	
	<p class="FormCampo"><label>Palavras chaves:<br />
	<input type="text" name="not_keywords" value="${noticia.not_keywords}" /></label></p>
	
	<p class="FormCampo"><label>Autor:<br />
	<input type="text" name="not_autor" value="${noticia.not_autor}" /></label></p>
	
	<p class="FormCampo"><label>Foto para destaque:<br />
	<input type="file" name="not_imgdest" /></label></p>
	
	<p class="FormCampo"><label>*Not�cia:<br />
	<textarea rows="12" name="not_texto">${noticia.not_texto}</textarea></label></p>	
	
	
	
	<p><input type="submit" value="Atualizar Not�cia" class="FormBotao" /></p>

</form>

<ckeditor:replace  replace="not_texto" basePath="../ckeditor/" />
<p>&nbsp;</p>
<p>*Preenchimento obrigat�rio</p>
<p>&nbsp;</p>