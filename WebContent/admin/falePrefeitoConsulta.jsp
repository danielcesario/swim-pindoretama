<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="myTags" tagdir="/WEB-INF/tags"%> 
	<h1>Fale com o Prefeito</h1>
	<h2>- Consulta -</h2>
	<c:if test="${param.controle ne 1}">
		<form action="index.jsp?pag=falePrefeitoConsulta&controle=1" method="post">
		<p> <label class="fpf_con">Filtro<input type="text" name="filtro"></label>
			<select name="campo">
				<option value="data"> Data </option>
				<option value="remetente"> Remetente </option>
				<option value="email"> E-mail </option>
				<option value="estado"> Estado </option>
				<option value="setor"> Setor  </option>		
			</select>
			<input type="submit" value="Filtrar"></p>
		</form>	
		<a href="falePrefeitoAdm.jsp"> Administração </a><br>
		Lista de Entradas
		<hr>
	<myTags:fpfConsulta/>
	</c:if>
	
	
 <!-- ============================================================================================== -->
 
 	
	<c:if test="${param.controle eq 1}">
		<form action="index.jsp?pag=falePrefeitoConsulta&controle=1" method="post">
		<p> <label class="fpf_con">Filtro<input type="text" name="filtro" value="${param.filtro}"></label>
			<select name="campo">
				<option value="data"> Data </option>
				<option value="remetente"> Remetente </option>
				<option value="email"> E-mail </option>
				<option value="estado"> Estado </option>
				<option value="setor"> Setor  </option>		
			</select>
			<input type="submit" value="Filtrar"></p>
		</form>	
		<a href="falePrefeitoAdm.jsp"> Administração </a><br>
		Lista de Entradas
		<hr>
		<c:if test="${param.campo eq 'data' }">
			<myTags:fpfConsulta campo="${param.filtro}" filtro="data"/>
		</c:if>
		
		<c:if test="${param.campo eq 'remetente' }">
			<myTags:fpfConsulta campo="${param.filtro}" filtro="remetente" />
		</c:if>
		
		<c:if test="${param.campo eq 'email' }">
			<myTags:fpfConsulta campo="${param.filtro}" filtro="email" />
		</c:if>
		
		<c:if test="${param.campo eq 'estado' }">
			<myTags:fpfConsulta campo="${param.filtro}" filtro="estado" />
		</c:if>
		
		<c:if test="${param.campo eq 'setor' }">
			<myTags:fpfConsulta campo="${param.filtro}" filtro="setor" />
		</c:if>
		
		
	</c:if>
	