<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<jsp:useBean id="foto" class="model.Foto" scope="request"></jsp:useBean>  
  
<div id="admin_content">
	<jsp:useBean id="listafotos" class="model.dao.FotosDao" scope="page"></jsp:useBean>
	<h1>Edite a foto</h1>
	
	<c:if test="${not empty msgAlerta}">
		<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;"><b>${msgAlerta}</b></p>
	</c:if>
	<br>
	
	<p class="FormCampo">Os campos com * s�o obrigat�rios:</p>
	
	<c:forEach var="ft" items="${listafotos.consulta}">
		<c:if test="${ft.idFoto eq param.id}">
			<c:set var="foto" value="${ft}" scope="page"/>
		</c:if>			
	
	</c:forEach>
	<form action="editarFoto.do" method="post" >
	<p class="FormCampo"><img src="exibirFoto.do?id=${foto.idFoto}" width="200"></p><br>
	<p class="FormCampo"><label class='foto_campo'>T�tulo*:<br><input type="text" name='titulo' size='40' value="${foto.titulo}"></label></p>	
	<p class="FormCampo"><label class='foto_campo'>�lbum*:<br><input type="text" name='album' size='40' value="${foto.album}"></label></p>	
	<p class="FormCampo"><label class='foto_campo'>Descri��o:<br><TEXTAREA rows=5 cols=40 name="descricao">${foto.descricao}</TEXTAREA><br></label></p>
	<p class="FormCampo"><input type="hidden" name="idUsr" value="${sessionScope.usr_id}"></p>  <!-- ${sessionScope.usr_id} --> 
	<p class="FormCampo"><input type="hidden" name="foto_id" value="${foto.idFoto}"></p>  <!-- ${foto.idFoto} --> 
	
	<p><input type="submit" value="Gravar" class="FormBotao"></p><br>
	</form>
</div>