<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Controle de Not�cias</h1>

<p>&nbsp;</p>

<h2><a href="index.jsp?pag=noticiaAdicionar"><img src="../imagens/painel/adicionar.png" /> Adicionar not�cia</a></h2>

<p>&nbsp;</p>

<table class="tabListar">
	<tr>
		<th style="width: 750px;">Not�cia</th>
		<th style="width: 150px;">Op��es</th>
	</tr>

 
<jsp:useBean id="dao" class="model.dao.DaoNoticia" scope="request"></jsp:useBean>
<c:forEach var="noticia" items="${dao.listNoticias}">
	<tr>
		<td><img src="imagemNoticia.do?id=${noticia.not_id}" width="70" height="70" style="float: left; margin-right: 5px;"/>
		<b>${noticia.not_titulo}</b><br />
		${noticia.dataHoraFormatada}</td>
		<td style="text-align: center;">		
		<a href="editarNoticia.do?not_id=${noticia.not_id}"><img src="../imagens/painel/editar.png" /></a>
		<a href="excluirNoticia.do?not_id=${noticia.not_id}"><img src="../imagens/painel/delete.png" /></a>
		</td>
	</tr>
</c:forEach>
		
</table>

<p>&nbsp;</p>