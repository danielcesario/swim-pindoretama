<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

	<h1> Eventos </h1>
	<p>&nbsp;</p>
	<h2>Novo Evento</h2>
		<p>&nbsp;</p>
<c:if test="${not empty msgAlerta}">
	<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;line-height: 25px;"><b>${msgAlerta}</b></p>
</c:if>
	<p>&nbsp;</p>
	<c:set var="evento" value="${requestScope.evento}" scope="page"/>	
	<form action="cadastrarEvento.do" method="post" enctype="multipart/form-data">
	<table width="800" border="0">
	<tr>
		<td><label class='evt_campo'>T�tulo:</td> 
		<td><input type="text" name='titulo' size='40' value="${evento.titulo}" ></label></td>
	</tr>
	
	<tr>
		<td><label class='evt_campo'>Data:</td>
		<td><input type="text" name='data' size='20' value="<f:formatDate pattern="dd/MM/yyyy" value="${evento.data}"/>"></label></td>
	</tr>
	
	<tr>
		<td><label class='evt_campo'>Imagem:</td>
		<td><input type="file" name='imagem'></label></td>
	</tr>
	
	<tr>
		<td><label class='evt_campo'>Descri��o:</td>
		<td><TEXTAREA rows=15 cols=40 name="descricao">${evento.descricao}</TEXTAREA><br></label></td>
	</tr>
	</table>
	
	<input type="hidden" name="idUser" value="${sessionScope.usr_id}">
	
	<input type="submit" value="Gravar">
	
	</form>
</body>
</html>