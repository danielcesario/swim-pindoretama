<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="listaFotos" class="model.dao.FotosDao" scope="page"/>
<c:forEach var="ft" items="${listaFotos.consulta}">	
	<c:if test="${ft.idFoto eq param.id}">
		<img src="exibirFoto.do?id=${ft.idFoto}" width="400" height="400">
	</c:if>
</c:forEach>
</body>
</html>