<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<h1>Lista de Banner</h1>

<p>&nbsp;</p>

<h2><a href="index.jsp?pag=bannerCadastro"><img src="../imagens/painel/adicionar.png" /> Novo Banner</a></h2>

<p>&nbsp;</p>
	
<table class="tabListar">
	<tr>
		<th style="width: 100px;">Publica��o</th>
		<th style="width: 650px;">Lista de Eventos Cadastrados como Banner</th>
		<th style="width: 150px;">Op��es</th>
	</tr>


<jsp:useBean id="dao" class="model.dao.DaoBanner" scope="request"></jsp:useBean>
<c:forEach var="banner" items="${dao.listBan}">
	<tr>
		<c:if test="${banner.ban_publicacao ne 1}">
			<td style="text-align: center;"><input type="checkbox" name="publicado" value="ok" /></td> 		
		</c:if>
		<c:if test="${banner.ban_publicacao eq 1 }">
		<td style="text-align: center;"><input type="checkbox" name="publicado" value="ok" checked /></td> 
		</c:if>
		
		<td><b>${banner.ban_id} - ${banner.ban_nome}</b></td>
		
		<td style="text-align: center;">
			<a href="editarBanner.do?ban_id=${banner.ban_id}"><img src="../imagens/painel/editar.png" /></a>
			<a href="excluirBanner.do?ban_id=${banner.ban_id}"><img src="../imagens/painel/delete.png" /></a>
		</td>
	
	</tr>
</c:forEach>
			

	</table>

<p>&nbsp;</p>