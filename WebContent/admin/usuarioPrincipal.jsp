<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Controle de Usu�rios</h1>

<p>&nbsp;</p>

<h2><a href="index.jsp?pag=usuarioAdicionar"><img src="../imagens/painel/adicionar.png" /> Adicionar usu�rio</a></h2>

<p>&nbsp;</p>

<table class="tabListar">
	<tr>
		<th style="width: 750px;">Usu�rio</th>
		<th style="width: 150px;">Op��es</th>
	</tr>

 
<jsp:useBean id="dao" class="model.dao.DaoUsuario" scope="request"></jsp:useBean>
<c:forEach var="usr" items="${dao.listUser}">
	<tr>
		<td><b>${usr.usr_nome}</b><br />
		${usr.usr_email} - ${usr.usr_telefone}</td>
		<td style="text-align: center;">
		<a href="editarUsuario.do?usr_id=${usr.usr_id}"><img src="../imagens/painel/editar.png" /></a>
		<a href="excluirUsuario.do?usr_id=${usr.usr_id}"><img src="../imagens/painel/delete.png" /></a>
		</td>
	</tr>
</c:forEach>
		
</table>

<p>&nbsp;</p>