<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



			<div id="admin_content">
			
				<jsp:useBean id="dadosUtilidade" class="model.Utilidade" scope="request"></jsp:useBean>
				
				<h2>Cadastro de Empresas / Utilidade P�blica</h2>
				
				<c:if test="${not empty msgAlerta}">
					<p style="color: red; border: 1px solid red; margin: 30px; padding: 10px;line-height: 25px;"><b>${msgAlerta}</b></p>
				</c:if>
				<br>
				<p class="FormCampo">Os campos com * s�o obrigat�rios:</p>

				<form action="utilidadeCadastrar.do" method="post" enctype="multipart/form-data">

					<p class="FormCampo"> <label>Nome da Empresa*:<br /> <input type="text" name="up_nomeEmpresa" value="${dadosUtilidade.nomeEmpresa}"/> </label> </p>

					<p class="FormCampo"> <label>Endere�o*:<br /> <input type="text" name="up_endereco" value="${dadosUtilidade.endereco}"/> </label> </p>

					<p class="FormCampo"> <label>Telefone:<br /> <input type="text" name="up_telefone" value="${dadosUtilidade.telefone}"/> </label> </p>

					<p class="FormCampo"> <label>Logotipo:<br /> <input type="file" name="up_logotipo" /> </label> </p>

					<p class="FormCampo"> <label>P�gina na Web:<br /> http://<input type="text" name="up_url" value="${dadosUtilidade.url}"/></label> </p>
					
					<p class="FormCampo"><input type="hidden" name="usr_id" value="${sessionScope.usr_id}"></p>

					<p> <input type="submit" value="Cadastrar Utilidade" class="FormBotao" /> </p><br>

				</form>
			</div>
			