<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SWIM Pindoretama - Painel de Administra��o</title>
<link href="../estilos/principal.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="tudo">
	
	<div id="header">
		<jsp:include page="cabecalho.jsp"></jsp:include>
	</div>
	
	<div id="admin_content">		

		
			<%--- Choose para escolher a p�gina apropriada --%>		
			<c:choose>
				<c:when test='${empty sessionScope.usr_id}'>
					<jsp:include page="login.jsp"></jsp:include>
				</c:when >
				
				<c:when test='${empty sessionScope.usr_id and param["pag"] eq "ErroLogin"}'>
					<jsp:include page="errologin.jsp"></jsp:include>
				</c:when >

				<c:when test='${not empty sessionScope.usr_id and empty param["pag"]}'>
					<jsp:include page="principal.jsp"></jsp:include>
				</c:when >
				
				<c:when test='${not empty sessionScope.usr_id}'>
					<jsp:include page="${param.pag}.jsp"></jsp:include>
				</c:when >											
				
				<c:otherwise>
					<h1>Nenhuma p�gina selecionada</h1>
				</c:otherwise>				
			</c:choose >

	</div>
	
	<div id="footer">
		<jsp:include page="rodape.jsp"></jsp:include>
	</div>

</div>
</body>
</html>