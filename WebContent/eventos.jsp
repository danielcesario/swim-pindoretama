<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="eventos" scope="page" class="model.dao.EventosDao"/>
	<h1>Eventos</h1>
	
	<h2>Clique sobre o evento para ver maiores detalhes</h2>
	
	<p>&nbsp;</p>	
	
	<c:forEach var="evento" items="${eventos.consulta}">		
		<p class="linkEventos"><a href="index.jsp?pag=eventosVisCompleta&id=${evento.idEvento}"><f:formatDate pattern="dd/MM/yyyy" value="${evento.data}"/> - ${evento.titulo} </a></p>		
	</c:forEach>			