<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="noticia" class="model.Noticia" scope="request"></jsp:useBean>
	
	<h1>${noticia.not_titulo}</h1>
	<p>${noticia.dataHoraFormatada}</p>
	<p>&nbsp;</p>
	<p>${noticia.not_texto}</p>