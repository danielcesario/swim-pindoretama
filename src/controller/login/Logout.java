package controller.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Logout extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		
		HttpSession sessao = req.getSession();		
		sessao.removeAttribute("usr_id");
		sessao.removeAttribute("usr_nome");
		sessao.invalidate();
		resp.sendRedirect("index.jsp");
		
	}	

}
