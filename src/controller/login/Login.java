package controller.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.DaoUsuario;

public class Login extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String usuario = req.getParameter("usuario");
		String senha = req.getParameter("senha");
		
		DaoUsuario usr = new DaoUsuario();
		
		if(usr.getUser(usuario) != null){
			
			if(usr.getUser(usuario).getUsr_senha().equals(senha)){
			
				HttpSession sessao = req.getSession();
				sessao.setAttribute("usr_id", usr.getUser(usuario).getUsr_id());
				sessao.setAttribute("usr_nome", usr.getUser(usuario).getUsr_nome());
				resp.sendRedirect("index.jsp");
			
			} else {
				
				String msgAlerta = "Senha Inv�lida";
				req.setAttribute("msgAlerta", msgAlerta);
				RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=login");
				rd.forward(req, resp);				
				
			}
			
		} else {
			
			String msgAlerta = "Usu�rio Inv�lido";
			req.setAttribute("msgAlerta", msgAlerta);
			RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=login");
			rd.forward(req, resp);
			
		}
		
	}

}
