package controller.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Usuario;
import model.dao.DaoUsuario;

public class UsuarioEditar extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {
		
		DaoUsuario dao = new DaoUsuario();
		
		Usuario editar = dao.getUserById(Integer.parseInt(req.getParameter("usr_id")));
		
		req.setAttribute("usuario", editar);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=usuarioEditar");
		
		rd.forward(req, resp);
		
	}

}
