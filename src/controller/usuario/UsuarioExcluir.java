package controller.usuario;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.DaoUsuario;

public class UsuarioExcluir extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {
		
		DaoUsuario dao = new DaoUsuario();
		
		dao.delUser(Integer.parseInt(req.getParameter("usr_id")));
		
		resp.sendRedirect("index.jsp?pag=usuarioPrincipal");
		
	}

}
