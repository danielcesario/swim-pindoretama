package controller.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Usuario;
import model.dao.DaoUsuario;

public class UsuarioAdicionar extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		
		DaoUsuario dao = new DaoUsuario();
		Usuario usr = new Usuario();
		
		usr.setUsr_nome(req.getParameter("usr_nome"));
		usr.setUsr_email(req.getParameter("usr_email"));
		usr.setUsr_telefone(req.getParameter("usr_telefone"));
		usr.setUsr_login(req.getParameter("usr_login"));
		usr.setUsr_senha(req.getParameter("usr_senha"));
		
		
		String msgAlerta = "";
		
		if(usr.getUsr_nome() == null || usr.getUsr_nome().isEmpty()){			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de NOME vazio <br>";			
		} 		
		
		if(usr.getUsr_email() == null || usr.getUsr_email().isEmpty()) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de EMAIL vazio <br>";						
		} 
		
		if(usr.getUsr_login() == null || usr.getUsr_login().isEmpty()) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de LOGIN vazio <br>";												
		} 
		
		if(usr.getUsr_senha() == null || usr.getUsr_senha().isEmpty()) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de SENHA vazio <br>";				
		} 	
					
		if(msgAlerta == null || msgAlerta.isEmpty()) {	
			dao.addUser(usr);			
			resp.sendRedirect("index.jsp?pag=usuarioPrincipal");		
		} else {			
			req.setAttribute("msgAlerta", msgAlerta);
			req.setAttribute("dadosUsuario", usr);
			RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=usuarioAdicionar");
			rd.forward(req, resp);						
		}		
		
		
	}	

}
