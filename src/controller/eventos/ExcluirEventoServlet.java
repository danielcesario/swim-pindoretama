package controller.eventos;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.EventosDao;

public class ExcluirEventoServlet extends HttpServlet{
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		EventosDao dao = new EventosDao();
		int idEvento =Integer.parseInt(req.getParameter("id"));
		dao.delete(idEvento);
		resp.sendRedirect("index.jsp?pag=eventosConsulta");

	}
}
