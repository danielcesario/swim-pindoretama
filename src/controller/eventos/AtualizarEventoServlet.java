package controller.eventos;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import model.Eventos;
import model.dao.EventosDao;

public class AtualizarEventoServlet extends HttpServlet {
	
	Date dataBanco=null;
	public static byte[] fileItemToBytes(FileItem file){
		InputStream uploadedStream = null;
		byte[] bytes = null;

		try {
			uploadedStream = file.getInputStream();
			bytes = new byte[uploadedStream.available()];
			uploadedStream.read(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bytes;
		}		

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		EventosDao dao = new EventosDao();
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);    	
		try {			
			List items = upload.parseRequest(req);
			FileItem evt_titulo = (FileItem) items.get(0);
			String titulo = evt_titulo.getString();
	
			FileItem evt_data = (FileItem) items.get(1);
			String data = evt_data.getString();
				
			FileItem evt_descricao = (FileItem) items.get(2);
			String descricao = evt_descricao.getString();
	
			FileItem evt_idUser = (FileItem) items.get(3);
			int idUser = Integer.parseInt(evt_idUser.getString());	
			
			FileItem evt_id = (FileItem) items.get(4);
			int idEvt = Integer.parseInt(evt_id.getString());	
	 
			//formatar a data nos moldes do banco
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");     
			
			if(data.equals("") ||data == null ){
				dataBanco = null;
			}
			else{
				try{
				dataBanco = new java.sql.Date(dateFormat.parse(data).getTime());
				}catch (Exception e) {
					dataBanco=null;
				}
			}
			
			Eventos evento = new Eventos();
			evento.setIdEvento(idEvt);
			evento.setTitulo(titulo);
			evento.setData(dataBanco);			
			evento.setDescricao(descricao);	 
			evento.setIdUser(idUser);
			
			String msgAlerta = "";
    		
    		if(evento.getTitulo() == null || evento.getTitulo().isEmpty()){			
    			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo titulo vazio <br>";			
    		} 		
    			
    		if(evento.getData() == null ){			
    			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo data nulo <br>";			
    		} 		
    		   		    		
    		if(evento.getDescricao() == null || evento.getDescricao().isEmpty()) {			
    			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de descri��o vazio <br>";												
    		} 		
    							
    		if(msgAlerta == null || msgAlerta.isEmpty()) {	
    			dao.update(evento);
    			resp.sendRedirect("index.jsp?pag=eventosPrincipal");					
    		} else {			
    			req.setAttribute("msgAlerta", msgAlerta);
    			req.setAttribute("evento", evento);
    			RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=eventosEditar&id="+idEvt);
    			rd.forward(req, resp);						
    		}			
									
	
			} catch (FileUploadException e) {
				e.printStackTrace();
			}
	
	}
}
