package controller.eventos;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.db.ConnectionFactory;
import org.apache.tomcat.util.http.fileupload.FileItem;

public class ExibirEventoServlet extends HttpServlet {
	 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			    throws ServletException, IOException {			        			        
			        try {			 
			        	int id = Integer.parseInt(request.getParameter("id"));			        	
			        	Connection con = ConnectionFactory.createConnection();
			            PreparedStatement ps = con.prepareStatement("select evt_imagem from eventos where evt_idEventos=?");			            			            			            
			            ps.setInt(1, id);
			            ResultSet rs = ps.executeQuery();
			            rs.next();			            			            	            		
			            byte[] arquivo = rs.getBytes(1);
			            response.setContentType("image/jpeg");			          
			            OutputStream os = response.getOutputStream();			           			            
			            os.write(arquivo);	
			            os.close();			            			            
			        }
			        catch(Exception ex) {
			             System.out.println(ex.getMessage());
			        }
			    } 

			    @Override
			    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			    throws ServletException, IOException {
			        processRequest(request, response);
			    } 
			    @Override
			    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			    throws ServletException, IOException {
			        processRequest(request, response);
			    }			
}
