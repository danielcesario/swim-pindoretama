package controller.falePrefeito;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.FalePrefeitoDao;

public class ExcluirPrefeitoServlet extends HttpServlet{
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		FalePrefeitoDao dao = new FalePrefeitoDao();
		int idPrefeito = Integer.parseInt(req.getParameter("id"));
		dao.delete(idPrefeito);
		resp.sendRedirect("index.jsp?pag=falePrefeitoAdm");
	}
}
