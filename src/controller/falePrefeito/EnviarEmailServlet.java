package controller.falePrefeito;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Prefeito;
import model.dao.FalePrefeitoDao;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class EnviarEmailServlet extends HttpServlet {

	Date data;
	String remetente;
	String telefone;
	String email;
	String descricao;
	String setor;
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {	
		try {				
			Integer idEvento=Integer.parseInt(req.getParameter("id"));
			Integer idUsr=Integer.parseInt(req.getParameter("usr_id"));
			String setor = req.getParameter("setor");
			setMsg(idEvento,idUsr,setor);				
			resp.sendRedirect("index.jsp?pag=falePrefeitoAdm");
		     
		} catch (EmailException e) {				
			e.printStackTrace();
		}	
	
	}
		
	public void enviaEmailSimples(Date data,String remetente,String telefone,String e_mail,String descricao,String setor) throws EmailException {  
		SimpleDateFormat dataFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleEmail email = new SimpleEmail();        
        email.setHostName("smtp.gmail.com"); // o servidor SMTP para envio do e-mail  
        email.addTo("emaildelp3@gmail.com", "Ricardo"); //destinat�rio  
        //email.addCc("ariovaldojunior@gmail.com", "Ari"); // c�pia
        email.setFrom("emaildelp3@gmail.com", "Administrador Swim-Pindoretama"); // remetente  
        email.setSubject("Fale com o Prefeito - "+setor); // assunto do e-mail  
        email.setMsg("Mensagem recebida em: "+dataFormat.format(data)+ "\n"+
        			 "Enviada por: "+remetente+ "\n"+
        			 "Telefone: "+telefone+ "\n" +
        			 "E-mail: "+e_mail + "\n" +
        			 "Mensagem: "+descricao);   
        email.setAuthentication("emaildelp3","senhalp3" ); // usuario e senha        
        email.setSmtpPort(465);
        email.setSSL(true);  
        email.setTLS(true);
        email.send();     
    } 

 	private void setMsg(int idEvento,int idUsr,String set) throws EmailException{
 		FalePrefeitoDao dao = new FalePrefeitoDao();
 		Prefeito prefeito = dao.consultaUnica(idEvento);
 		data = prefeito.getData();
 		remetente = prefeito.getRemetente();
 		telefone = prefeito.getTelefone();
 		email = prefeito.getEmail();
 		descricao = prefeito.getDescricao();
 		setor = set; 	
 		if (remetente==null || remetente.equals(""))
 			remetente="N�o identificado";
 		if (telefone==null || telefone.equals(""))
 			telefone="N�o informado";
 		if (email==null || email.equals(""))
 			email="N�o informado"; 		
 		enviaEmailSimples(data,remetente,telefone,email,descricao,setor);
 		dao.mudaStatus(idEvento, idUsr,setor);
 	}
}

