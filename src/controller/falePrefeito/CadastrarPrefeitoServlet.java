package controller.falePrefeito;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Prefeito;
import model.dao.FalePrefeitoDao;

public class CadastrarPrefeitoServlet extends HttpServlet {

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		FalePrefeitoDao dao = new FalePrefeitoDao();
		Prefeito prefeito = new Prefeito();
		String nome=req.getParameter("nome");
		String telefone=req.getParameter("telefone");
		String email=req.getParameter("email");
		String descricao=req.getParameter("descricao");
		prefeito.setRemetente(nome);
		prefeito.setTelefone(telefone);
		prefeito.setEmail(email);
		prefeito.setDescricao(descricao);				
				
		
	String msgAlerta = "";
		
		if(prefeito.getRemetente() == null || prefeito.getRemetente().isEmpty()){			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo nome vazio <br>";			
		} 		
			
		if(prefeito.getDescricao() == null || prefeito.getDescricao().isEmpty()) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de DESCRI��O vazio <br>";												
		} 		
							
		if(msgAlerta == null || msgAlerta.isEmpty()) {	
			dao.add(prefeito);
			resp.sendRedirect("index.jsp?pag=cadastrarFalePrefeito&enviado=1");					
		} else {			
			req.setAttribute("msgAlerta", msgAlerta);
			req.setAttribute("prefeito", prefeito);
			RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=cadastrarFalePrefeito");
			rd.forward(req, resp);						
		}
	}
}
