package controller.banner;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Banner;
import model.dao.DaoBanner;

public class BannerEditar extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {
		
		DaoBanner dao = new DaoBanner();
		
		Banner editar = dao.getBanById(Integer.parseInt(req.getParameter("ban_id")));
		
		req.setAttribute("banner", editar);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=bannerEditar");
		
		rd.forward(req, resp);
		
	}

}
