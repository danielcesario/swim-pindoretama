package controller.banner;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import model.dao.DaoBanner;

public class BannerExcluir extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {
		
		DaoBanner dao = new DaoBanner();
		
		dao.delBan(Integer.parseInt(req.getParameter("ban_id")));
		
		resp.sendRedirect("index.jsp?pag=bannerPrincipal");
		
	}

}
