package controller.banner;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Banner;
import model.dao.DaoBanner;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

public class BannerUpdateImg extends HttpServlet {
	public static byte[] fileItemToBytes(FileItem file){
		InputStream uploadedStream = null;
		byte[] bytes = null;

		try {
			uploadedStream = file.getInputStream();
			bytes = new byte[uploadedStream.available()];
			uploadedStream.read(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bytes;
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			
			DaoBanner dao = new DaoBanner();
			Banner banner = new Banner();
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			
			try {		
							
				List items = upload.parseRequest(req);

				FileItem ban_imagem = (FileItem) items.get(0);
				byte[] imgdest = fileItemToBytes(ban_imagem);
				banner.setBan_imagem(imgdest);
				
				FileItem ban_nome = (FileItem) items.get(1);
				String nome = ban_nome.getString();
				banner.setBan_nome(nome);
				
				FileItem ban_origem = (FileItem) items.get(2);
				String origem = ban_origem.getString();
				banner.setBan_origem(origem);
				
				FileItem ban_contato_tel = (FileItem) items.get(3);
				String tel = ban_contato_tel.getString();
				banner.setBan_contato_tel(tel);
				
				FileItem ban_contato_site = (FileItem) items.get(4);
				String site = ban_contato_site.getString();
				banner.setBan_contato_site(site);
				
				FileItem ban_descricao = (FileItem) items.get(5);
				String desc = ban_descricao.getString();
				banner.setBan_descricao(desc);

				FileItem ban_publicacao = (FileItem) items.get(6);
				String pub = ban_publicacao.getString();
				banner.setBan_publicacao(pub);

				FileItem user_id = (FileItem) items.get(7);
				String usr = user_id.getString();
				banner.setUser_id(usr);
				
				FileItem ban_id = (FileItem) items.get(8);
				String id = ban_id.getString();
				banner.setBan_id(Integer.parseInt(id));
				
				int var_erro = 0;		// tratamento de erro
				
				if (banner.getBan_nome() == null || banner.getBan_nome().isEmpty())
					var_erro += 1;
				if (banner.getBan_origem() == null || banner.getBan_origem().isEmpty())
					var_erro += 1;
				if (banner.getBan_contato_tel() == null || banner.getBan_contato_tel().isEmpty())
					var_erro += 1;
				if (banner.getBan_contato_site() == null || banner.getBan_contato_site().isEmpty())
					var_erro += 1;
				if (banner.getBan_descricao() == null || banner.getBan_descricao().isEmpty())
					var_erro += 1;
				if (banner.getBan_publicacao() == null || banner.getBan_publicacao().isEmpty())
					var_erro += 1;
					
				if (var_erro > 0) {
					resp.sendRedirect("index.jsp?pag=paginaErro"); 
				}
				else{
					dao.updtBanNoPic(banner);
					resp.sendRedirect("index.jsp?pag=bannerPrincipal");
				}
			
			} catch (FileUploadException e) {
				e.printStackTrace();
			}
			
		}		

}