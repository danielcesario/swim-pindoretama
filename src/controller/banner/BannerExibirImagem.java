package controller.banner;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.db.ConnectionFactory;

public class BannerExibirImagem extends HttpServlet{
	 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			    throws ServletException, IOException {			        			        
			        try {			 
			        	int id = Integer.parseInt(request.getParameter("id"));
			        	Connection con = ConnectionFactory.createConnection();
			            PreparedStatement ps = con.prepareStatement("SELECT ban_imagem FROM banner WHERE ban_id = ?");			            			            			            
			            ps.setInt(1, id);
			            ResultSet rs = ps.executeQuery();
			            rs.next();			            			            	            		
			            byte[] arquivo = rs.getBytes(1);
			            response.setContentType("image/jpeg");			          
			            OutputStream os = response.getOutputStream();			           			            
			            os.write(arquivo);	
			            os.close();			            			            
			        }
			        catch(Exception ex) {
			             //System.out.println(ex.getMessage());
			             ex.printStackTrace();
			        }
			    }
	 @Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 processRequest(req, resp);
	}
	 @Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 processRequest(req, resp);
	}
	 
}
