package controller.itinerario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Itinerario;
import model.dao.DaoItinerario;

public class ItinerarioEditar extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {
		
		DaoItinerario dao = new DaoItinerario();
		
		Itinerario editar = dao.getLinhaById(Integer.parseInt(req.getParameter("it_id")));
		
		req.setAttribute("itinerario", editar);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=editarItinerario");
		
		rd.forward(req, resp);
		
	}

}
