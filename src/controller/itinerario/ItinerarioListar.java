package controller.itinerario;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Itinerario;
import model.dao.DaoItinerario;

public class ItinerarioListar extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {
		
		DaoItinerario dao = new DaoItinerario();
		
		Itinerario listar = dao.getLinhaById(Integer.parseInt(req.getParameter("it_id")));
		
		req.setAttribute("itinerario", listar);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=consultar_linha");
		
		rd.forward(req, resp);
		
	}

}
