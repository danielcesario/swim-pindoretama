package controller.itinerario;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import model.dao.DaoItinerario;

public class ItinerarioExcluir extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
		throws IOException, ServletException {
		
		DaoItinerario dao = new DaoItinerario();
		
		dao.delLinha(Integer.parseInt(req.getParameter("it_id")));
		
		resp.sendRedirect("index.jsp?pag=itinerarioPrincipal");
		
	}

}
