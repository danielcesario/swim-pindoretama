package controller.itinerario;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Itinerario;
import model.dao.DaoItinerario;

public class ItinerarioAdicionar extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		
		DaoItinerario dao = new DaoItinerario();
		Itinerario itinerario = new Itinerario();
		
				
		
		
		itinerario.setIt_num_linha(req.getParameter("it_num_linha"));
		itinerario.setIt_itinerario(req.getParameter("it_itinerario"));
		itinerario.setIt_horario_saida(req.getParameter("it_horario_saida"));
		itinerario.setIt_horario_chegada(req.getParameter("it_horario_chegada"));
		itinerario.setIt_descricao(req.getParameter("it_descricao"));
		//itinerario.setIt_ultima_atualizacao(req.getParameter("it_ultima_atualizacao"));  esta sendo adicionado na DAO, portanto comentei
		itinerario.setUser_id(req.getParameter("user_id"));
		
		int var_erro = 0;

		if(itinerario.getIt_num_linha() == null || itinerario.getIt_num_linha() == "")
			var_erro += 1;
		if (itinerario.getIt_itinerario() == null || itinerario.getIt_itinerario() == "")
			var_erro += 1;
		if (itinerario.getIt_horario_saida() == null || itinerario.getIt_horario_saida() == "")
			var_erro += 1;
		if (itinerario.getIt_horario_chegada() == null || itinerario.getIt_horario_chegada() == "")
			var_erro += 1;
		if (itinerario.getIt_descricao() == null || itinerario.getIt_descricao() == "")
			var_erro += 1;
			
		if (var_erro > 0) {
			resp.sendRedirect("index.jsp?pag=paginaErro"); 
		}
		else{
			dao.addLinha(itinerario);
			resp.sendRedirect("index.jsp?pag=itinerarioPrincipal");
		}
	}	

}
