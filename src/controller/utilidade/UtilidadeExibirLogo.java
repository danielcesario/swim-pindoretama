package controller.utilidade;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.db.ConnectionFactory;
import org.apache.tomcat.util.http.fileupload.FileItem;

public class UtilidadeExibirLogo extends HttpServlet {
	
	protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
				try{
					int id = Integer.parseInt(req.getParameter("id"));
					Connection con = ConnectionFactory.createConnection();
					String sql ="SELECT up_logotipo FROM utilidadepublica WHERE up_id=?";  
					PreparedStatement st = con.prepareStatement(sql);
					st.setInt(1, id);
					ResultSet rs = st.executeQuery();
					
					rs.next();
					byte[] arquivo = rs.getBytes(1);
					resp.setContentType("image/jpeg");
					OutputStream os = resp.getOutputStream();
					os.write(arquivo);
					os.close();
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
					

	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
				processRequest(req, resp);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
				processRequest(req, resp);
	}

}
