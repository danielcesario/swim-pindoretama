package controller.utilidade;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Utilidade;
import model.dao.UtilidadeDao;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

public class UtilidadeEditar extends HttpServlet {
	
	public static byte[] fileItemToBytes(FileItem file){
		InputStream uploadedStream = null;
		byte[] bytes = null;

		try {
			uploadedStream = file.getInputStream();
			bytes = new byte[uploadedStream.available()];
			uploadedStream.read(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bytes;
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		
		UtilidadeDao dao = new UtilidadeDao();
		Utilidade utilidade = new Utilidade();
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
			
		try {		
						
			List items = upload.parseRequest(req);
			
			FileItem up_id = (FileItem) items.get(0);
			String id = up_id.getString();
			utilidade.setId(Integer.parseInt(id));
			
			FileItem up_nomeEmpresa = (FileItem) items.get(1);
			String nomeEmpresa = up_nomeEmpresa.getString();
			utilidade.setNomeEmpresa(nomeEmpresa);
			
			FileItem up_endereco = (FileItem) items.get(2);
			String endereco = up_endereco.getString();
			utilidade.setEndereco(endereco);
			
			FileItem up_telefone = (FileItem) items.get(3);
			String telefone = up_telefone.getString();
			utilidade.setTelefone(telefone);
			
			FileItem up_logotipo = (FileItem) items.get(4);
			byte[] logotipo = fileItemToBytes(up_logotipo);
			utilidade.setLogotipo(logotipo);
			
			FileItem up_url = (FileItem) items.get(5);
			String url = up_url.getString();
			utilidade.setUrl(url);
			
			FileItem up_userID = (FileItem) items.get(6);
			String userID = up_userID.getString();
			utilidade.setUserId(Integer.parseInt(userID));				

		
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		
				
		String msgAlerta = "";
		
		if(utilidade.getNomeEmpresa() == null || utilidade.getNomeEmpresa().isEmpty()){			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo 'Nome da Empresa' vazio <br>";			
		} 		

		if(utilidade.getEndereco() == null || utilidade.getEndereco().isEmpty()) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo 'Endere�o' vazio <br>";												
		} 
		
		if(msgAlerta == null || msgAlerta.isEmpty()) {	
			dao.atualizarUtilidade(utilidade);		
			resp.sendRedirect("index.jsp?pag=utilidade_listar");		
		} else {			
			req.setAttribute("msgAlerta", msgAlerta);
			req.setAttribute("dadosUtilidade", utilidade);
			RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=utilidadecadastro");
			rd.forward(req, resp);						
		}
		
	}	

}
