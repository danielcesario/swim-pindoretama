package controller.utilidade;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.UtilidadeDao;

public class UtilidadeExcluir extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		UtilidadeDao dao = new UtilidadeDao();
		
		dao.delUtilidade(Integer.parseInt(req.getParameter("up_id")));
		resp.sendRedirect("index.jsp?pag=utilidade_listar");
		}


}



