package controller.utilidade;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Utilidade;
import model.dao.UtilidadeDao;

public class UtilidadeExibir extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		UtilidadeDao dao = new UtilidadeDao();
		
		Utilidade utilidade = dao.getUtilidadeById(Integer.parseInt(req.getParameter("up_id")));
		
		req.setAttribute("utilidade", utilidade);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=utilidade_editar");
		
		rd.forward(req, resp);
		
	}

}
