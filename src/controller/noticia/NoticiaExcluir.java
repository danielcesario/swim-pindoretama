package controller.noticia;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.DaoNoticia;

public class NoticiaExcluir extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		DaoNoticia dao = new DaoNoticia();
		
		dao.delNoticia(Integer.parseInt(req.getParameter("not_id")));
		
		resp.sendRedirect("index.jsp?pag=noticiaPrincipal");
		
	}

}
