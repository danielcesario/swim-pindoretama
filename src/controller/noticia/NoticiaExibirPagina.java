package controller.noticia;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Noticia;
import model.dao.DaoNoticia;

public class NoticiaExibirPagina extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		DaoNoticia dao = new DaoNoticia();
		
		List<Noticia> not = dao.getListNoticiasTodas(Integer.parseInt(req.getParameter("pag")));
		
		req.setAttribute("noticia", not);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=noticiaExibeTodas");
		
		rd.forward(req, resp);
		
	}

}
