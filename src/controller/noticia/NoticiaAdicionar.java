package controller.noticia;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Noticia;
import model.dao.DaoNoticia;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

public class NoticiaAdicionar extends HttpServlet {
	
	public static byte[] fileItemToBytes(FileItem file){
		InputStream uploadedStream = null;
		byte[] bytes = null;

		try {
			uploadedStream = file.getInputStream();
			bytes = new byte[uploadedStream.available()];
			uploadedStream.read(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bytes;
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		
		DaoNoticia dao = new DaoNoticia();
		Noticia not = new Noticia();
		Date dataAtual = new Date();
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
			
		try {		
						
			List items = upload.parseRequest(req);
			
			FileItem not_titulo = (FileItem) items.get(0);
			String titulo = not_titulo.getString();
			not.setNot_titulo(titulo);
			
			FileItem not_descricao = (FileItem) items.get(1);
			String descricao = not_descricao.getString();
			not.setNot_descricao(descricao);
			
			FileItem not_keywords = (FileItem) items.get(2);
			String keywords = not_keywords.getString();
			not.setNot_keywords(keywords);
			
			FileItem not_autor = (FileItem) items.get(3);
			String autor = not_autor.getString();
			not.setNot_autor(autor);
			
			FileItem not_imgdest = (FileItem) items.get(4);
			byte[] imgdest = fileItemToBytes(not_imgdest);
			not.setNot_imgdest(imgdest);
			
			FileItem not_texto = (FileItem) items.get(5);
			String texto = not_texto.getString();
			not.setNot_texto(texto);
			
			FileItem usr_id = (FileItem) items.get(6);
			String usr = usr_id.getString();
			not.setUser_id(Integer.parseInt(usr));
			
			not.setNot_data(dataAtual);		
		
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		
				
		String msgAlerta = "";
		
		if(not.getNot_titulo() == null || not.getNot_titulo().isEmpty()){			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de T�TULO vazio <br>";			
		} 		
		
		if(not.getNot_imgdest() == null || not.getNot_imgdest().length == 0) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de IMAGEM vazio <br>";						
		} 
		
		if(not.getNot_descricao() == null || not.getNot_descricao().isEmpty()) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de DESCRI��O vazio <br>";												
		} 
		
		if(not.getNot_texto() == null || not.getNot_texto().isEmpty()) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo de NOT�CIA vazio <br>";				
		} 	
					
		if(msgAlerta == null || msgAlerta.isEmpty()) {	
			dao.addNoticia(not);		
			resp.sendRedirect("index.jsp?pag=noticiaPrincipal");		
		} else {			
			req.setAttribute("msgAlerta", msgAlerta);
			req.setAttribute("dadosNoticia", not);
			RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=noticiaAdicionar");
			rd.forward(req, resp);						
		}
		
	}	

}
