package controller.noticia;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Noticia;
import model.dao.DaoNoticia;

public class NoticiaExibir extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		DaoNoticia dao = new DaoNoticia();
		
		Noticia not = dao.getNoticiaById(Integer.parseInt(req.getParameter("not_id")));
		
		req.setAttribute("noticia", not);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=noticiaExibe");
		
		rd.forward(req, resp);
		
	}

}
