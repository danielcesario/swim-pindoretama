package controller.noticia;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Noticia;
import model.dao.DaoNoticia;

public class NoticiaBusca extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		DaoNoticia dao = new DaoNoticia();
		
		List<Noticia> not = dao.getListNoticiasBusca(req.getParameter("palavra"));				
		
		if(req.getParameter("palavra") == null || req.getParameter("palavra").isEmpty()){
			
			req.setAttribute("msgAlerta", "Nenhuma palavra digitada");
			
		} else if(not.size() == 0){
			
			req.setAttribute("msgAlerta", "Nenhum resultado encontrado");
			
		} else {
			
			req.setAttribute("msgAlerta", "Encontrado(s) "+ not.size() +" resultado(s)");
			req.setAttribute("noticiasBusca", not);
			
		}
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=buscaNoticias");
		
		rd.forward(req, resp);
		
	}

}
