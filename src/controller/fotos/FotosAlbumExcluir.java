package controller.fotos;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.FotosDao;

public class FotosAlbumExcluir extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		FotosDao dao = new FotosDao();
		String album = req.getParameter("album");
		dao.deleteAlbum(album);
		resp.sendRedirect("index.jsp?pag=fotos_exibir");
	}
}
