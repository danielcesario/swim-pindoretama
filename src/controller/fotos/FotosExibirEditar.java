package controller.fotos;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Foto;
import model.dao.FotosDao;

public class FotosExibirEditar extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		FotosDao dao = new FotosDao();
		
		Foto foto = dao.getFotosById(Long.parseLong(req.getParameter("foto_id")));
		
		req.setAttribute("foto", foto);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=fotos_editar");
		
		rd.forward(req, resp);
		
	}

}
