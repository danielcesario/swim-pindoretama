package controller.fotos;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Foto;
import model.dao.FotosDao;

public class FotosExibirAlbum extends HttpServlet {
	
	public void doGet (HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		FotosDao dao = new FotosDao();
		
		List<Foto> fotos = dao.getConsultaFotosPorAlbum(req.getParameter("foto_album"));
		
		req.setAttribute("fotos", fotos);
		
		RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=fotos_album");
		
		rd.forward(req, resp);
		
	}

}
