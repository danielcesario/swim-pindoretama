package controller.fotos;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Foto;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import model.dao.FotosDao;


public class FotosCadastrar extends HttpServlet {
	
	public static byte[] fileItemToBytes(FileItem file){
		InputStream uploadedStream = null;
		byte[] bytes = null;

		try {
			uploadedStream = file.getInputStream();
			bytes = new byte[uploadedStream.available()];
			uploadedStream.read(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bytes;
		}		
		
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {    	   
    	FotosDao dao = new FotosDao();
    	FileItemFactory factory = new DiskFileItemFactory();
    	ServletFileUpload upload = new ServletFileUpload(factory);   
    	Foto foto = new Foto();
		try {			
			List items = upload.parseRequest(req);
			
			System.out.println("Este � o item: " + items.get(0));
			System.out.println("Este � o item: " + items.get(1));
			System.out.println("Este � o item: " + items.get(2));
			System.out.println("Este � o item: " + items.get(3));
			System.out.println("Este � o item: " + items.get(4));
			System.out.println("Este � o item: " + items.get(5));
			System.out.println("Este � o item: " + items.get(6));
			
			String titulo = "";
			String album = "";
			byte[] imagem = null;
			String descricao = "";
			int idUser = 0;
			
			
			FileItem teste = (FileItem) items.get(1);
			String opcao = teste.getString();
			if(opcao.equals("1")){
				
				FileItem foto_titulo = (FileItem) items.get(0);
				titulo = foto_titulo.getString();
				
				FileItem foto_album = (FileItem) items.get(2);
				album = foto_album.getString();
				
				FileItem foto_imagem = (FileItem) items.get(4);
				imagem = fileItemToBytes(foto_imagem);
				
				FileItem foto_descricao = (FileItem) items.get(5);
				descricao = foto_descricao.getString();
				
				FileItem foto_idUser = (FileItem) items.get(6);
				idUser = Integer.parseInt(foto_idUser.getString());	
				
			}else{
				
				FileItem foto_titulo = (FileItem) items.get(0);
				titulo = foto_titulo.getString();
				
				FileItem foto_album = (FileItem) items.get(3);
				album = foto_album.getString();
				
				FileItem foto_imagem = (FileItem) items.get(4);
				imagem = fileItemToBytes(foto_imagem);
				
				FileItem foto_descricao = (FileItem) items.get(5);
				descricao = foto_descricao.getString();
				
				FileItem foto_idUser = (FileItem) items.get(6);
				idUser = Integer.parseInt(foto_idUser.getString());	
				
			}
			
			
			foto.setTitulo(titulo);
			foto.setAlbum(album);
			foto.setImagem(imagem);
			foto.setDescricao(descricao);
			foto.setUsr_id(idUser);
			
	    	//dao.add(foto);
	    	
		} catch (FileUploadException e) {
			e.printStackTrace();
		}    
		
		
		String msgAlerta = "";
		
		if(foto.getTitulo() == null || foto.getTitulo().isEmpty()){			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo 'T�tulo' vazio <br>";			
		} 		

		if(foto.getImagem() == null || foto.getImagem().length == 0) {			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo 'Imagem' vazio <br>";												
		}
		
		if(foto.getAlbum() == null || foto.getAlbum().isEmpty()){			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo '�lbum' vazio <br>";			
		}
		
		if(msgAlerta == null || msgAlerta.isEmpty()) {	
			dao.add(foto);		
			resp.sendRedirect("index.jsp?pag=fotos_exibir");		
		} else {			
			req.setAttribute("msgAlerta", msgAlerta);
			req.setAttribute("dadosFoto", foto);
			RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=fotos_cadastrar");
			rd.forward(req, resp);						
		}
		

    }

}
