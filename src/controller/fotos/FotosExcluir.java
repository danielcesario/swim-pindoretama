package controller.fotos;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.FotosDao;

public class FotosExcluir extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		FotosDao dao = new FotosDao();
		int fotoId = Integer.parseInt(req.getParameter("id"));
		dao.delete(fotoId);
		resp.sendRedirect("index.jsp?pag=fotos_exibir");
	}
}
