package controller.fotos;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Foto;
import model.dao.FotosDao;

public class FotosEditar extends HttpServlet {

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		Foto foto = new Foto();
		FotosDao dao = new FotosDao();
		foto.setIdFoto(Long.parseLong(req.getParameter("foto_id")));
		foto.setTitulo(req.getParameter("titulo"));
		foto.setAlbum(req.getParameter("album"));
		foto.setDescricao(req.getParameter("descricao"));
		foto.setUsr_id(Integer.parseInt(req.getParameter("idUsr")));
		//dao.update(foto);
		
		String msgAlerta = "";
		
		if(foto.getTitulo() == null || foto.getTitulo().isEmpty()){			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo 'T�tulo' vazio <br>";			
		} 		

		//if(foto.getImagem() == null || foto.getImagem().length == 0) {			
		//	msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo 'Imagem' vazio <br>";												
		//}
		
		if(foto.getAlbum() == null || foto.getAlbum().isEmpty()){			
			msgAlerta += "<img src='../imagens/painel/alerta.png' /> Campo '�lbum' vazio <br>";			
		}
		
		if(msgAlerta == null || msgAlerta.isEmpty()) {	
			dao.update(foto);		
			resp.sendRedirect("index.jsp?pag=fotos_exibir");		
		} else {			
			req.setAttribute("msgAlerta", msgAlerta);
			req.setAttribute("foto", foto);
			RequestDispatcher rd = req.getRequestDispatcher("index.jsp?pag=fotos_editar");
			rd.forward(req, resp);						
		}
		
	}
	
	
}
