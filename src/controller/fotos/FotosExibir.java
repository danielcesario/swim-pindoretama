package controller.fotos;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.db.ConnectionFactory;

public class FotosExibir extends HttpServlet {
	 protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
			    throws ServletException, IOException {			        			        
			        try {			 
			        	int id = Integer.parseInt(req.getParameter("id"));			        	
			        	Connection con = ConnectionFactory.createConnection();
			            PreparedStatement ps = con.prepareStatement("Select foto_imagem from foto where foto_id=?");			            			            			            
			            ps.setInt(1, id);
			            ResultSet rs = ps.executeQuery();
			            rs.next();			            			            	            		
			            byte[] arquivo = rs.getBytes(1);
			            resp.setContentType("image/jpeg");			          
			            OutputStream os = resp.getOutputStream();			           			            
			            os.write(arquivo);	
			            os.close();			            			            
			        }
			        catch(Exception ex) {
			             System.out.println(ex.getMessage());
			        }
			    } 

			    
			    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			    throws ServletException, IOException {
			        processRequest(req, resp);
			    } 
			    
			    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			    throws ServletException, IOException {
			        processRequest(req, resp);
			    }
}
