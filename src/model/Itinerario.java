package model;

import java.sql.Date;

public class Itinerario {
	
	private int it_id;
	private String it_num_linha;
	private String it_itinerario;
	private String it_horario_saida;
	private String it_horario_chegada;
	private String it_descricao;
	private Date it_ultima_atualizacao;
	private String user_id;
	
	

	public Itinerario() {
		
	}
	
	
	

	public int getIt_id() {
		return it_id;
	}
	public void setIt_id(int it_id) {
		this.it_id = it_id;
	}


	public String getIt_num_linha() {
		return it_num_linha;
	}
	public void setIt_num_linha(String it_num_linha) {
		this.it_num_linha = it_num_linha;
	}


	public String getIt_itinerario() {
		return it_itinerario;
	}
	public void setIt_itinerario(String it_itinerario) {
		this.it_itinerario = it_itinerario;
	}


	public String getIt_horario_saida() {
		return it_horario_saida;
	}
	public void setIt_horario_saida(String it_horario_saida) {
		this.it_horario_saida = it_horario_saida;
	}


	public String getIt_horario_chegada() {
		return it_horario_chegada;
	}
	public void setIt_horario_chegada(String it_horario_chegada) {
		this.it_horario_chegada = it_horario_chegada;
	}


	public String getIt_descricao() {
		return it_descricao;
	}
	public void setIt_descricao(String it_descricao) {
		this.it_descricao = it_descricao;
	}
	
	
	public Date getIt_ultima_atualizacao() {
		return it_ultima_atualizacao;
	}
	public void setIt_ultima_atualizacao(Date string) {
		this.it_ultima_atualizacao = string;
	}

	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	

}
