package model;

public class Usuario {
	
	private int usr_id;
	private String usr_nome;
	private String usr_email;
	private String usr_telefone;
	private String usr_login;
	private String usr_senha;
	
	
	
	public Usuario() {
		
	}
	
	public int getUsr_id() {
		return usr_id;
	}
	public void setUsr_id(int usrId) {
		usr_id = usrId;
	}
	public String getUsr_nome() {
		return usr_nome;
	}
	public void setUsr_nome(String usrNome) {
		usr_nome = usrNome;
	}
	public String getUsr_email() {
		return usr_email;
	}
	public void setUsr_email(String usrEmail) {
		usr_email = usrEmail;
	}
	public String getUsr_telefone() {
		return usr_telefone;
	}
	public void setUsr_telefone(String usrTelefone) {
		usr_telefone = usrTelefone;
	}
	public String getUsr_login() {
		return usr_login;
	}
	public void setUsr_login(String usrLogin) {
		usr_login = usrLogin;
	}
	public String getUsr_senha() {
		return usr_senha;
	}
	public void setUsr_senha(String usr_senha) {
		this.usr_senha = usr_senha;
	}
	
	

}
