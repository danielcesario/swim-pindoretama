package model;

public class Foto {
	private long idFoto;
	private String titulo;
	private byte[] imagem;
	private String descricao;
	private String album;
	private int usr_id;
	
	
	public long getIdFoto() {
		return idFoto;
	}
	public void setIdFoto(long idFoto) {
		this.idFoto = idFoto;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public byte[] getImagem() {
		return imagem;
	}
	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public int getUsr_id() {
		return usr_id;
	}
	public void setUsr_id(int usr_id) {
		this.usr_id = usr_id;
	}
	
	
}
