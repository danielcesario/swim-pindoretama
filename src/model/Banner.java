package model;

import java.sql.Date;

public class Banner {
	
	private int ban_id;
	private String ban_nome;
	private String ban_origem;
	private String ban_contato_tel;
	private String ban_contato_site;
	private String ban_descricao;
	private Date ban_ultima_atualizacao;
	private String ban_publicacao;
	private byte[] ban_imagem;
	private String user_id;
	
	
	
	public Banner() {
		
	}
	
	

	public int getBan_id() {
		return ban_id;
	}
	public void setBan_id(int ban_id) {
		this.ban_id = ban_id;
	}

	public String getBan_nome() {
		return ban_nome;
	}
	public void setBan_nome(String ban_nome) {
		this.ban_nome = ban_nome;
	}

	public String getBan_origem() {
		return ban_origem;
	}
	public void setBan_origem(String ban_origem) {
		this.ban_origem = ban_origem;
	}

	public String getBan_contato_tel() {
		return ban_contato_tel;
	}
	public void setBan_contato_tel(String ban_contato_tel) {
		this.ban_contato_tel = ban_contato_tel;
	}

	public String getBan_contato_site() {
		return ban_contato_site;
	}
	public void setBan_contato_site(String ban_contato_site) {
		this.ban_contato_site = ban_contato_site;
	}

	public String getBan_descricao() {
		return ban_descricao;
	}
	public void setBan_descricao(String ban_descricao) {
		this.ban_descricao = ban_descricao;
	}

	public Date getBan_ultima_atualizacao() {
		return ban_ultima_atualizacao;
	}
	public void setBan_ultima_atualizacao(Date ban_ultima_atualizacao) {
		this.ban_ultima_atualizacao = ban_ultima_atualizacao;
	}

	public String getBan_publicacao() {
		return ban_publicacao;
	}
	public void setBan_publicacao(String ban_publicacao) {
		this.ban_publicacao = ban_publicacao;
	}

	public byte[] getBan_imagem() {
		return ban_imagem;
	}
	public void setBan_imagem(byte[] ban_imagem) {
		this.ban_imagem = ban_imagem;
	}

	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}



}
