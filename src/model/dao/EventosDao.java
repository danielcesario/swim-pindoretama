package model.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Eventos;
import model.db.ConnectionFactory;

import org.apache.tomcat.util.http.fileupload.FileItem;


public class EventosDao {
	FileItem file; 
	public boolean add(Eventos evento) throws IOException{
		int added=0;
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="insert into eventos (evt_titulo,evt_data,evt_imagem,evt_descricao,usr_id) values (?,?,?,?,?)";  
		PreparedStatement st = con.prepareStatement(sql);		
		st.setString(1, evento.getTitulo());		
		st.setDate(2, evento.getData()); 
		st.setBytes(3, evento.getImagem());
		st.setString(4, evento.getDescricao());
		st.setInt(5, 1); // COLOCAR DEPOIS O ID DO USUARIO LOGADO NA SESSAO  <<<<<<============================
		st.setInt(5, evento.getIdUser());
		added = st.executeUpdate();
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return added>0;
	}
	
	public int delete(int idEvento){
		int delete=0;
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="Delete from eventos where evt_idEventos=?";  
		PreparedStatement st = con.prepareStatement(sql);
		st.setInt(1, idEvento);		
		delete = st.executeUpdate();
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}
		return delete;
	}
	
//============================================================================	
	
	public boolean update(Eventos evento) throws IOException{
		int update=0;
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="UPDATE eventos SET evt_titulo=?,evt_data=?,evt_descricao=?,usr_id=? where evt_idEventos=? ";  
		PreparedStatement st = con.prepareStatement(sql);		
		st.setString(1, evento.getTitulo());		
		st.setDate(2, evento.getData()); 		
		st.setString(3, evento.getDescricao());		
		st.setInt(4, evento.getIdUser());
		st.setInt(5, evento.getIdEvento());		
		update = st.executeUpdate();
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return update>0;
	}
	
	public boolean updateFoto(Eventos evento) throws IOException{
		int update=0;
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="UPDATE eventos SET evt_titulo=?,evt_data=?,evt_imagem=?,evt_descricao=?,usr_id=? where evt_idEventos=? ";  
		PreparedStatement st = con.prepareStatement(sql);		
		st.setString(1, evento.getTitulo());		
		st.setDate(2, evento.getData()); 
		st.setBytes(3, evento.getImagem());
		st.setString(4, evento.getDescricao());		
		st.setInt(5, evento.getIdUser());
		st.setInt(6, evento.getIdEvento());		
		update = st.executeUpdate();
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return update>0;
	}

	
	
//======================================================
/*	public List<Eventos> consultaSimples(){		
		List<Eventos> eventos = new ArrayList<Eventos>();
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="Select evt_idEventos,evt_titulo,evt_data from eventos";  
		PreparedStatement st = con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		while(rs.next()){
			Eventos evento = new Eventos();
			evento.setIdEvento(rs.getInt("evt_idEventos"));		
			evento.setTitulo(rs.getString("evt_titulo"));		
			evento.setData(rs.getDate("evt_data"));			
			eventos.add(evento);
		}		
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}				
		return eventos;
	}
	
	public Eventos consultaUnica(int id){
		Eventos evento = new Eventos();
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="Select evt_titulo,evt_descricao from eventos where evt_idEventos=?";  
		PreparedStatement st = con.prepareStatement(sql);
		st.setInt(1,id);
		ResultSet rs = st.executeQuery();
		rs.next();							
		evento.setTitulo(rs.getString("evt_titulo"));		
		evento.setDescricao(rs.getString("evt_descricao"));								
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return evento;
	} */
	
	public List<Eventos> getConsulta(){		
		List<Eventos> eventos = new ArrayList<Eventos>();
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="SELECT * FROM eventos ORDER BY evt_data DESC";  
		PreparedStatement st = con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		while(rs.next()){
			Eventos evento = new Eventos();
			evento.setIdEvento(rs.getInt("evt_idEventos"));		
			evento.setTitulo(rs.getString("evt_titulo"));		
			evento.setData(rs.getDate("evt_data"));
			evento.setDescricao(rs.getString("evt_descricao"));
			eventos.add(evento);
		}		
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return eventos;
	}
	
	
	public List<Eventos> getConsultaLimite(){		
		List<Eventos> eventos = new ArrayList<Eventos>();
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="SELECT * FROM eventos ORDER BY evt_data DESC";  
		PreparedStatement st = con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		while(rs.next()){
			Eventos evento = new Eventos();
			evento.setIdEvento(rs.getInt("evt_idEventos"));		
			evento.setTitulo(rs.getString("evt_titulo"));		
			evento.setData(rs.getDate("evt_data"));
			evento.setDescricao(rs.getString("evt_descricao"));
			eventos.add(evento);
		}		
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return eventos;
	}

	
}

	
