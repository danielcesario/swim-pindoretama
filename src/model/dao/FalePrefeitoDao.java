package model.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Prefeito;
import model.db.ConnectionFactory;

public class FalePrefeitoDao {
	public boolean add(Prefeito prefeito) throws IOException{
		int added=0;
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="insert into faleprefeito (flp_data,flp_remetente,flp_telefone,flp_email,flp_descricao,flp_estado,flp_setor) values (?,?,?,?,?,?,?)";  
		PreparedStatement st = con.prepareStatement(sql);		
		st.setDate(1, new Date(System.currentTimeMillis())); // verificar a data depois
		st.setString(2, prefeito.getRemetente());
		st.setString(3, prefeito.getTelefone());
		st.setString(4, prefeito.getEmail());
		st.setString(5, prefeito.getDescricao());
		st.setString(6, "p");
		st.setString(7, "indefinido");
		added = st.executeUpdate();
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return added>0;
	}
	
	public int delete(int idPrefeito){
		int delete=0;
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="Delete from faleprefeito where flp_idfalePrefeito=?";  
		PreparedStatement st = con.prepareStatement(sql);
		st.setInt(1, idPrefeito);		
		delete = st.executeUpdate();
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}
		return delete;
	}
	
	public List<Prefeito> getList(){		
		List<Prefeito> prefeitos = new ArrayList<Prefeito>();
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="Select * from faleprefeito";  
		PreparedStatement st = con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		while(rs.next()){
			Prefeito prefeito = new Prefeito();
			prefeito.setIdPrefeito(rs.getInt("flp_idfalePrefeito"));
			prefeito.setData(rs.getDate("flp_data"));
			prefeito.setRemetente(rs.getString("flp_remetente"));
			prefeito.setTelefone(rs.getString("flp_telefone"));
			prefeito.setEmail(rs.getString("flp_email")); 
			prefeito.setDescricao(rs.getString("flp_descricao"));
			prefeito.setEstado(rs.getString("flp_estado"));
			prefeito.setSetor(rs.getString("flp_setor"));
//			evento.setUserId(rs.getInt("evt_userId"));
			prefeitos.add(prefeito);
		}		
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return prefeitos;
	}
	
		
	public List<Prefeito> getListPendentes(){		 // lista apenas as mensagens que tem o status de pendente
		List<Prefeito> prefeitos = new ArrayList<Prefeito>();
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="Select * from faleprefeito where flp_estado='p'";  
		PreparedStatement st = con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		while(rs.next()){
			Prefeito prefeito = new Prefeito();
			prefeito.setIdPrefeito(rs.getInt("flp_idfalePrefeito"));
			prefeito.setData(rs.getDate("flp_data"));
			prefeito.setRemetente(rs.getString("flp_remetente"));
			prefeito.setTelefone(rs.getString("flp_telefone"));
			prefeito.setEmail(rs.getString("flp_email")); 
			prefeito.setDescricao(rs.getString("flp_descricao"));
			prefeito.setEstado(rs.getString("flp_estado"));
			prefeito.setSetor(rs.getString("flp_setor"));
//			evento.setUserId(rs.getInt("evt_userId"));
			prefeitos.add(prefeito);
		}		
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return prefeitos;
	}
	
	public Prefeito consultaUnica(int id){				
		Prefeito prefeito = new Prefeito();
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="Select * from faleprefeito where flp_idfalePrefeito=?";  
		PreparedStatement st = con.prepareStatement(sql);
		st.setInt(1, id);
		ResultSet rs = st.executeQuery();
		rs.next();		
		prefeito.setIdPrefeito(rs.getInt("flp_idfalePrefeito"));
		prefeito.setData(rs.getDate("flp_data"));
		prefeito.setRemetente(rs.getString("flp_remetente"));
		prefeito.setTelefone(rs.getString("flp_telefone"));
		prefeito.setEmail(rs.getString("flp_email")); 
		prefeito.setDescricao(rs.getString("flp_descricao"));
		prefeito.setEstado(rs.getString("flp_estado"));
		prefeito.setSetor(rs.getString("flp_setor"));
//		evento.setUserId(rs.getInt("evt_userId"));		
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}		
		return prefeito;
	} 	
	
	public void mudaStatus(int id,int idUsr,String setor){						
		try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="UPDATE faleprefeito SET flp_estado=?,flp_setor=?,usr_id=? where flp_idfalePrefeito=?";		
		PreparedStatement st = con.prepareStatement(sql);
		st.setString(1,"e");  
		st.setString(2,setor);
		st.setInt(3,idUsr);
		st.setInt(4,id);
		st.executeUpdate();
		st.close();
		con.close();		
		} catch (SQLException e){
			e.printStackTrace();
		}				
	}	
}
