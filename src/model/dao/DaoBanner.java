package model.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Banner;
import model.db.ConnectionFactory;

public class DaoBanner {
	
	public Banner getBan(String ban_id){
		
		Banner banner = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM banner WHERE ban_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, ban_id);
			ResultSet rs = st.executeQuery();
			
			if(rs.next()){			
				
				banner = new Banner();
				banner.setBan_id(rs.getInt("ban_id"));
				banner.setBan_nome(rs.getString("ban_nome"));
				banner.setBan_descricao(rs.getString("ban_descricao"));
					
			}
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			}	
		
			return banner;
			
	}
			
	public Banner getBanById (int id){		// getbanner by ID (pega banner e suas informacoes da tabela)
		
		Banner banner = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM banner WHERE ban_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			
			if(rs.next()){
				
				banner = new Banner();
				banner.setBan_id(rs.getInt("ban_id"));
				banner.setBan_nome(rs.getString("ban_nome"));
				banner.setBan_origem(rs.getString("ban_origem"));
				banner.setBan_contato_tel(rs.getString("ban_contato_tel"));
				banner.setBan_contato_site(rs.getString("ban_contato_site"));
				banner.setBan_descricao(rs.getString("ban_descricao"));
				banner.setBan_ultima_atualizacao(rs.getDate("ban_ultima_atualizacao"));
				banner.setBan_publicacao(rs.getString("ban_publicacao"));
				banner.setUser_id(rs.getString("user_id"));
				
			}
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			}	
		
			return banner;
			
	}
		
	public List<Banner> getListBan(){		
		
		List<Banner> listaBanners = new ArrayList<Banner>();
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM banner ORDER BY ban_id";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				
				Banner banner = new Banner();
				banner.setBan_id(rs.getInt("ban_id"));
				banner.setBan_nome(rs.getString("ban_nome"));
				banner.setBan_origem(rs.getString("ban_origem"));
				banner.setBan_contato_tel(rs.getString("ban_contato_tel"));
				banner.setBan_contato_site(rs.getString("ban_contato_site"));
				banner.setBan_descricao(rs.getString("ban_descricao"));
				banner.setBan_ultima_atualizacao(rs.getDate("ban_ultima_atualizacao"));
				banner.setBan_publicacao(rs.getString("ban_publicacao"));
				banner.setUser_id(rs.getString("user_id"));
				listaBanners.add(banner);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}		
		
		return listaBanners;
		
	}
	
public List<Banner> getListBanHome(){		
	
	List<Banner> listaBanners = new ArrayList<Banner>();
	try{
		Connection con = ConnectionFactory.createConnection();
		String sql ="SELECT * FROM banner WHERE ban_publicacao = 1";  
		PreparedStatement st = con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		while(rs.next()){
			Banner banner = new Banner();
			banner.setBan_id(rs.getInt("ban_id"));
			banner.setBan_nome(rs.getString("ban_nome"));
			banner.setBan_origem(rs.getString("ban_origem"));
			banner.setBan_contato_tel(rs.getString("ban_contato_tel"));
			banner.setBan_contato_site(rs.getString("ban_contato_site"));
			banner.setBan_descricao(rs.getString("ban_descricao"));
			banner.setBan_ultima_atualizacao(rs.getDate("ban_ultima_atualizacao"));
			banner.setBan_publicacao(rs.getString("ban_publicacao"));
			banner.setUser_id(rs.getString("user_id"));
			listaBanners.add(banner);
		}
		st.close();
		con.close();
	} catch (SQLException e){
		e.printStackTrace();
	}		
	return listaBanners;
}
	
	
	public boolean addBan(Banner banner){
		int added=0;

		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="INSERT INTO banner (ban_nome,ban_origem,ban_contato_tel,ban_contato_site,ban_descricao,ban_ultima_atualizacao,ban_publicacao,ban_imagem,user_id) VALUES (?,?,?,?,?,?,?,?,?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, banner.getBan_nome());
			st.setString(2, banner.getBan_origem());
			st.setString(3, banner.getBan_contato_tel());
			st.setString(4, banner.getBan_contato_site());
			st.setString(5, banner.getBan_descricao());
			st.setDate(6, new Date(System.currentTimeMillis()));
			st.setString(7, banner.getBan_publicacao());
			st.setBytes(8, banner.getBan_imagem());
			st.setString(9, banner.getUser_id());
			
			added = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return added > 0;
	}	
		
	public int delBan(int ban_id){
		
		int delete=0;		
		try{			
			Connection con = ConnectionFactory.createConnection();
			String sql ="DELETE FROM banner WHERE ban_id=?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, ban_id);		
			delete = st.executeUpdate();
			
			st.close();
			con.close();			
		} catch (SQLException e){		
			e.printStackTrace();			
		}		
		return delete;		
	}
	
	
	public int updtBan(Banner banner){
		
		int update=0;

		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="UPDATE banner SET ban_nome=?, ban_origem=?, ban_contato_tel=?, ban_contato_site=?, ban_descricao=?, ban_ultima_atualizacao=?, ban_publicacao=?, ban_imagem=?, user_id=? WHERE ban_id=?";  
			PreparedStatement st = con.prepareStatement(sql);
			
			st.setString(1, banner.getBan_nome());
			st.setString(2, banner.getBan_origem());
			st.setString(3, banner.getBan_contato_tel());
			st.setString(4, banner.getBan_contato_site());
			st.setString(5, banner.getBan_descricao());
			st.setDate(6, new Date(System.currentTimeMillis()));
			st.setString(7, banner.getBan_publicacao());
			st.setBytes(8, banner.getBan_imagem());
			st.setString(9, banner.getUser_id());
			st.setInt(10, banner.getBan_id());
			
			update = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return update;
	}		
	
	
public int updtBanNoPic(Banner banner){
		
		int update=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="UPDATE banner SET ban_nome=?, ban_origem=?, ban_contato_tel=?, ban_contato_site=?, ban_descricao=?, ban_ultima_atualizacao=?, ban_publicacao=?, user_id=? WHERE ban_id=?";  
			PreparedStatement st = con.prepareStatement(sql);
			
			st.setString(1, banner.getBan_nome());
			st.setString(2, banner.getBan_origem());
			st.setString(3, banner.getBan_contato_tel());
			st.setString(4, banner.getBan_contato_site());
			st.setString(5, banner.getBan_descricao());
			st.setDate(6, new Date(System.currentTimeMillis()));
			st.setString(7, banner.getBan_publicacao());
			st.setString(8, banner.getUser_id());
			st.setInt(9, banner.getBan_id());
			
			update = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		
		
		return update;
	}

}
