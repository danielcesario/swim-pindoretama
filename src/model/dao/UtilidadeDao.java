package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Utilidade;
import model.db.ConnectionFactory;

public class UtilidadeDao {
	
	public Utilidade getUtilidadeById(int id){
		
		Utilidade util = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM utilidadepublica WHERE up_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			
			rs.next();
				
			util = new Utilidade();
			util.setId(rs.getLong("up_id"));
			util.setNomeEmpresa(rs.getString("up_nomeEmpresa"));
			util.setEndereco(rs.getString("up_endereco"));
			util.setTelefone(rs.getString("up_telefone"));
			util.setUrl(rs.getString("up_url"));
			util.setUserId(rs.getInt("usr_id"));
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			}
		
			return util;
			
	}
	
	public List<Utilidade> getListUtilidade(){		
		
		List<Utilidade> listaUtilidade = new ArrayList<Utilidade>();
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM utilidadepublica ORDER BY up_nomeEmpresa";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){				
				
				Utilidade util = new Utilidade();
				util = new Utilidade();
				util.setId(rs.getLong("up_id"));
				util.setNomeEmpresa(rs.getString("up_nomeEmpresa"));
				util.setEndereco(rs.getString("up_endereco"));
				util.setTelefone(rs.getString("up_telefone"));
				util.setUrl(rs.getString("up_url"));
				util.setUserId(rs.getInt("usr_id"));

				listaUtilidade.add(util);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		} 
		
		return listaUtilidade;
		
	}
	
	public List<Utilidade> getListUtilidadeUser(){		
		
		List<Utilidade> listaUtilidade = new ArrayList<Utilidade>();
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM utilidadepublica ORDER BY up_nomeEmpresa";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){				
				
				Utilidade util = new Utilidade();
				util = new Utilidade();
				util.setId(rs.getLong("up_id"));
				util.setNomeEmpresa(rs.getString("up_nomeEmpresa"));
				util.setEndereco(rs.getString("up_endereco"));
				util.setTelefone(rs.getString("up_telefone"));
				util.setUrl(rs.getString("up_url"));
				util.setUserId(rs.getInt("usr_id"));

				listaUtilidade.add(util);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		} 
		
		return listaUtilidade;
		
	}
	
	public boolean addUtilidade(Utilidade utilidade){
		int added=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="INSERT INTO utilidadepublica (up_nomeEmpresa,up_endereco,up_telefone,up_logotipo,up_url,usr_id) VALUES (?,?,?,?,?,?)";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, utilidade.getNomeEmpresa());
			st.setString(2, utilidade.getEndereco());
			st.setString(3, utilidade.getTelefone());
			st.setBytes(4, utilidade.getLogotipo());
			st.setString(5, "http://" + utilidade.getUrl());
			st.setInt(6, utilidade.getUserId());//st.setInt(6, utilidade.getUserId());
			
			added = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return added > 0;
	}	
	
	
	public int delUtilidade(int id){
		
		int delete=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="DELETE FROM utilidadepublica WHERE up_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);		
			delete = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
		
			e.printStackTrace();
			
		}
		
		return delete;
		
	}
			
	public int atualizarUtilidade(Utilidade utilidade){
		
		int update=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			
			if(utilidade.getLogotipo().length == 0 || utilidade.getLogotipo() == null ){
				
				String sql ="UPDATE utilidadepublica SET up_nomeEmpresa=?, up_endereco=?, up_telefone=?, up_url=?, usr_id=? WHERE up_id=?";  
				PreparedStatement st = con.prepareStatement(sql);
				st.setString(1, utilidade.getNomeEmpresa());
				st.setString(2, utilidade.getEndereco());
				st.setString(3, utilidade.getTelefone());
				st.setString(4, utilidade.getUrl());
				st.setLong(6, utilidade.getId());
				st.setInt(5, utilidade.getUserId());
				update = st.executeUpdate();
				st.close();

				
			} else {
				
				String sql ="UPDATE utilidadepublica SET up_nomeEmpresa=?, up_endereco=?, up_telefone=?, up_logotipo=?, up_url=?, usr_id=? WHERE up_id=?";  
				PreparedStatement st = con.prepareStatement(sql);
				st.setString(1, utilidade.getNomeEmpresa());
				st.setString(2, utilidade.getEndereco());
				st.setString(3, utilidade.getTelefone());
				st.setBytes(4, utilidade.getLogotipo());
				st.setString(5, utilidade.getUrl());
				st.setLong(7, utilidade.getId());
				st.setInt(6, utilidade.getUserId());
				update = st.executeUpdate();
				st.close();
				
			
			}
																		
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return update;
	}		
	
}
