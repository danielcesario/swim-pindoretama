package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Noticia;
import model.db.ConnectionFactory;

public class DaoNoticia {
	
	public Noticia getNoticiaById(int id){
		
		Noticia not = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM noticia WHERE not_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			
			if(rs.next()){
				
					DateFormat dtOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
					Date data = dtOutput.parse(rs.getString("not_data")); 				
				
					not = new Noticia();
					not.setNot_titulo(rs.getString("not_titulo"));
					not.setNot_descricao(rs.getString("not_descricao"));
					not.setNot_keywords(rs.getString("not_keywords"));
					not.setNot_autor(rs.getString("not_autor"));
					not.setNot_texto(rs.getString("not_texto"));
					not.setNot_id(rs.getInt("not_id"));
					not.setNot_data(data);
					
			}
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			} catch (java.text.ParseException e) {
				
				e.printStackTrace();
				
			}	
		
			return not;
			
	}
	
	public List<Noticia> getListNoticias(){		
		
		List<Noticia> listaNoticias = new ArrayList<Noticia>();
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM noticia ORDER BY not_data DESC";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){				
				  
				DateFormat dtOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
				Date data = dtOutput.parse(rs.getString("not_data")); 
				
				Noticia not = new Noticia();
				not.setNot_id(rs.getInt("not_id"));
				not.setNot_titulo(rs.getString("not_titulo"));
				not.setNot_descricao(rs.getString("not_descricao"));
				not.setNot_keywords(rs.getString("not_keywords"));
				not.setNot_texto(rs.getString("not_texto"));
				not.setNot_autor(rs.getString("not_autor"));
				not.setNot_data(data);
				not.setUser_id(rs.getInt("user_id"));
				listaNoticias.add(not);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
			
		}
		return listaNoticias;
		
	}
	
	public List<Noticia> getListNoticiasTodas(int pag){		
		
		List<Noticia> listaNoticias = new ArrayList<Noticia>();
		int totalNotPag = 5;		
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM noticia ORDER BY not_data DESC LIMIT ?,?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, pag * totalNotPag);
			st.setInt(2, totalNotPag);
			ResultSet rs = st.executeQuery();
			
			
			while(rs.next()){				
				  
				DateFormat dtOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
				Date data = dtOutput.parse(rs.getString("not_data")); 
				
				Noticia not = new Noticia();
				not.setNot_id(rs.getInt("not_id"));
				not.setNot_titulo(rs.getString("not_titulo"));
				not.setNot_descricao(rs.getString("not_descricao"));
				not.setNot_keywords(rs.getString("not_keywords"));
				not.setNot_texto(rs.getString("not_texto"));
				not.setNot_autor(rs.getString("not_autor"));
				not.setNot_data(data);
				not.setUser_id(rs.getInt("user_id"));
				listaNoticias.add(not);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
			
		}
		return listaNoticias;
		
	}
	
	public List<Noticia> getListNoticiasBusca(String p){		
		
		List<Noticia> listaNoticias = new ArrayList<Noticia>();		
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM noticia WHERE not_texto LIKE ? ORDER BY not_data DESC";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, "%"+p+"%");
			ResultSet rs = st.executeQuery();			
			
			while(rs.next()){				
				  
				DateFormat dtOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
				Date data = dtOutput.parse(rs.getString("not_data")); 
				
				Noticia not = new Noticia();
				
				not.setNot_id(rs.getInt("not_id"));
				not.setNot_titulo(rs.getString("not_titulo"));
				not.setNot_descricao(rs.getString("not_descricao"));
				not.setNot_keywords(rs.getString("not_keywords"));
				not.setNot_texto(rs.getString("not_texto"));
				not.setNot_autor(rs.getString("not_autor"));
				not.setNot_data(data);
				not.setUser_id(rs.getInt("user_id"));
				
				listaNoticias.add(not);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
			
		}
		
		return listaNoticias;
		
}
	
	public List<Integer> getNumeroPaginas(){		
		
	List<Integer> totalPag = new ArrayList<Integer>();
	
		int totalNot = 0;
		int totalNotPag = 5;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM noticia";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				
				totalNot = totalNot + 1;
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();			
			
		}
		
		for(int i = 0; i < totalNot/totalNotPag + 1; i++){
			totalPag.add(i);
		}
		
		return totalPag;
		
	}	
	
	public List<Noticia> getListNoticiasHome(){		
		
		List<Noticia> listaNoticias = new ArrayList<Noticia>();
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM noticia ORDER BY not_data DESC LIMIT 6";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){				
				  
				DateFormat dtOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
				Date data = dtOutput.parse(rs.getString("not_data")); 
				
				Noticia not = new Noticia();
				not.setNot_id(rs.getInt("not_id"));
				not.setNot_titulo(rs.getString("not_titulo"));
				not.setNot_descricao(rs.getString("not_descricao"));
				not.setNot_keywords(rs.getString("not_keywords"));
				not.setNot_texto(rs.getString("not_texto"));
				not.setNot_autor(rs.getString("not_autor"));
				not.setNot_data(data);
				not.setUser_id(rs.getInt("user_id"));
				listaNoticias.add(not);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
			
		}
		return listaNoticias;
		
	}	
				
	public boolean addNoticia(Noticia noticia){
		int added=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="INSERT INTO noticia (not_titulo,not_descricao,not_keywords,not_imgdest,not_autor,not_texto,not_data,user_id) VALUES (?,?,?,?,?,?,?,?)";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, noticia.getNot_titulo());
			st.setString(2, noticia.getNot_descricao());
			st.setString(3, noticia.getNot_keywords());
			st.setBytes(4, noticia.getNot_imgdest());
			st.setString(5, noticia.getNot_autor());
			st.setString(6, noticia.getNot_texto());
			st.setString(7, noticia.getNot_data());
			st.setInt(8, noticia.getUser_id());
			
			added = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return added > 0;
	}	
	
	public int delNoticia(int id){
		
		int delete=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="DELETE FROM noticia WHERE not_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);		
			delete = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
		
			e.printStackTrace();
			
		}
		
		return delete;
		
	}
			
	public int updtNoticia(Noticia not){
		
		int update=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			
			if(not.getNot_imgdest() == null || not.getNot_imgdest().length == 0){
				
				String sql ="UPDATE noticia SET not_titulo=?, not_descricao=?, not_keywords=?, not_autor=?, not_texto=? WHERE not_id=?";  
				PreparedStatement st = con.prepareStatement(sql);
				st.setString(1, not.getNot_titulo());
				st.setString(2, not.getNot_descricao());
				st.setString(3, not.getNot_keywords());
				st.setString(4, not.getNot_autor());
				st.setString(5, not.getNot_texto());
				st.setInt(6, not.getNot_id());
				update = st.executeUpdate();
				st.close();
				
			} else {
			
				String sql ="UPDATE noticia SET not_titulo=?, not_descricao=?, not_keywords=?, not_autor=?, not_imgdest=?, not_texto=? WHERE not_id=?";  
				PreparedStatement st = con.prepareStatement(sql);
				st.setString(1, not.getNot_titulo());
				st.setString(2, not.getNot_descricao());
				st.setString(3, not.getNot_keywords());
				st.setString(4, not.getNot_autor());
				st.setBytes(5, not.getNot_imgdest());
				st.setString(6, not.getNot_texto());
				st.setInt(7, not.getNot_id());
				update = st.executeUpdate();
				st.close();
			
			}
																		
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return update;
	}		
	
}
