package model.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Itinerario;
import model.db.ConnectionFactory;

public class DaoItinerario {
	
	public Itinerario getLinha(String it_id){		// getLinha (servia pra logar o usuario)
		
		Itinerario itinerario = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM itinerario WHERE it_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, it_id);
			ResultSet rs = st.executeQuery();
			
			if(rs.next()){			
				
				itinerario = new Itinerario();
				itinerario.setIt_id(rs.getInt("it_id"));
				itinerario.setIt_num_linha(rs.getString("it_num_linha"));
				itinerario.setIt_horario_saida(rs.getString("it_horario_saida"));
					
			}
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			}	
		
			return itinerario;
			
	}
			
	public Itinerario getLinhaById (int it_id){		// getLinha by ID (pega linha e suas informacoes da tabela)
		
		Itinerario itinerario = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM itinerario WHERE it_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, it_id);
			ResultSet rs = st.executeQuery();
			
			if(rs.next()){
				
				itinerario = new Itinerario();
				itinerario.setIt_id(rs.getInt("it_id"));
				itinerario.setIt_num_linha(rs.getString("it_num_linha"));
				itinerario.setIt_itinerario(rs.getString("it_itinerario"));
				itinerario.setIt_horario_saida(rs.getString("it_horario_saida"));
				itinerario.setIt_horario_chegada(rs.getString("it_horario_chegada"));
				itinerario.setIt_descricao(rs.getString("it_descricao"));
				itinerario.setIt_ultima_atualizacao(rs.getDate("it_ultima_atualizacao"));
				itinerario.setUser_id(rs.getString("user_id"));
				
			}
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			}	
		
			return itinerario;
			
	}
		
	public List<Itinerario> getListLinha(){		
		
		List<Itinerario> listaItinerarios = new ArrayList<Itinerario>();
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM itinerario ORDER BY it_num_linha";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				
				Itinerario itinerario = new Itinerario();
				itinerario.setIt_id(rs.getInt("it_id"));
				itinerario.setIt_num_linha(rs.getString("it_num_linha"));
				itinerario.setIt_itinerario(rs.getString("it_itinerario"));
				itinerario.setIt_horario_saida(rs.getString("it_horario_saida"));
				itinerario.setIt_horario_chegada(rs.getString("it_horario_chegada"));
				itinerario.setIt_descricao(rs.getString("it_descricao"));
				itinerario.setIt_ultima_atualizacao(rs.getDate("it_ultima_atualizacao"));
				itinerario.setUser_id(rs.getString("user_id"));
				listaItinerarios.add(itinerario);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}		
		
		return listaItinerarios;
		
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public List<Itinerario> getListSelect(int it_num_linha){		// esse metodo funcionou mas nao precisou ser usado
		
		List<Itinerario> listaSelectItinerarios = new ArrayList<Itinerario>();		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM itinerario WHERE it_num_linha = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, it_num_linha);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){				
				Itinerario itinerario = new Itinerario();
				itinerario.setIt_id(rs.getInt("it_id"));
				itinerario.setIt_num_linha(rs.getString("it_num_linha"));
				itinerario.setIt_itinerario(rs.getString("it_itinerario"));
				itinerario.setIt_horario_saida(rs.getString("it_horario_saida"));
				itinerario.setIt_horario_chegada(rs.getString("it_horario_chegada"));
				itinerario.setIt_descricao(rs.getString("it_descricao"));
				itinerario.setIt_ultima_atualizacao(rs.getDate("it_ultima_atualizacao"));
				itinerario.setUser_id(rs.getString("user_id"));
				listaSelectItinerarios.add(itinerario);				
			}			
			st.close();
			con.close();
			
		} catch (SQLException e){			
			e.printStackTrace();			
		}
		return listaSelectItinerarios;		
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public boolean addLinha(Itinerario itinerario){
		int added=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="INSERT INTO itinerario (it_num_linha,it_itinerario,it_horario_saida,it_horario_chegada,it_descricao,it_ultima_atualizacao,user_id) VALUES (?,?,?,?,?,?,?)";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, itinerario.getIt_num_linha());
			st.setString(2, itinerario.getIt_itinerario());
			st.setString(3, itinerario.getIt_horario_saida());
			st.setString(4, itinerario.getIt_horario_chegada());
			st.setString(5, itinerario.getIt_descricao());
			st.setDate(6, new Date(System.currentTimeMillis()));
			st.setString(7, itinerario.getUser_id());
			
			added = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return added > 0;
	}	
		
	public int delLinha(int it_id){
		
		int delete=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="DELETE FROM itinerario WHERE it_id=?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, it_id);		
			delete = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
		
			e.printStackTrace();
			
		}
		
		return delete;
		
	}
			
	public int updtLinha(Itinerario itinerario){
		
		int update=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="UPDATE itinerario SET it_num_linha=?, it_itinerario=?, it_horario_saida=?, it_horario_chegada=?, it_descricao=?, it_ultima_atualizacao=?, user_id=? WHERE it_id=?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, itinerario.getIt_num_linha());
			st.setString(2, itinerario.getIt_itinerario());
			st.setString(3, itinerario.getIt_horario_saida());
			st.setString(4, itinerario.getIt_horario_chegada());
			st.setString(5, itinerario.getIt_descricao());
			st.setDate(6, new Date(System.currentTimeMillis()));
			st.setString(7, itinerario.getUser_id());
			st.setInt(8, itinerario.getIt_id());
			
			update = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return update;
	}		
	
}
