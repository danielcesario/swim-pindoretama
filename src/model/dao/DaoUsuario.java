package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Usuario;
import model.db.ConnectionFactory;

public class DaoUsuario {
	
	public Usuario getUser(String login){
		
		Usuario usuario = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM usuario WHERE usr_login = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, login);
			ResultSet rs = st.executeQuery();
			
			if(rs.next()){			
				
					usuario = new Usuario();
					usuario.setUsr_id(rs.getInt("usr_id"));
					usuario.setUsr_nome(rs.getString("usr_nome"));
					usuario.setUsr_senha(rs.getString("usr_senha"));
					
			}
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			}	
		
			return usuario;
			
	}
			
	public Usuario getUserById (int id){
		
		Usuario usuario = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM usuario WHERE usr_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			
			if(rs.next()){			
				
					usuario = new Usuario();
					usuario.setUsr_id(rs.getInt("usr_id"));
					usuario.setUsr_nome(rs.getString("usr_nome"));
					usuario.setUsr_email(rs.getString("usr_email"));
					usuario.setUsr_telefone(rs.getString("usr_telefone"));
					usuario.setUsr_login(rs.getString("usr_login"));
					usuario.setUsr_senha(rs.getString("usr_senha"));
					
			}
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			}	
		
			return usuario;
			
	}
		
	public List<Usuario> getListUser(){		
		
		List<Usuario> listaUsuarios = new ArrayList<Usuario>();
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM usuario ORDER BY usr_nome";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				
				Usuario usuario = new Usuario();
				usuario.setUsr_id(rs.getInt("usr_id"));
				usuario.setUsr_nome(rs.getString("usr_nome"));
				usuario.setUsr_email(rs.getString("usr_email"));
				usuario.setUsr_telefone(rs.getString("usr_telefone"));
				usuario.setUsr_login(rs.getString("usr_login"));
				usuario.setUsr_senha(rs.getString("usr_senha"));
				listaUsuarios.add(usuario);
				
			}
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}		
		
		return listaUsuarios;
		
	}		
	
	public boolean addUser(Usuario usuario){
		int added=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="INSERT INTO usuario (usr_nome,usr_email,usr_telefone,usr_login,usr_senha) VALUES (?,?,?,?,?)";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, usuario.getUsr_nome());
			st.setString(2, usuario.getUsr_email());
			st.setString(3, usuario.getUsr_telefone());
			st.setString(4, usuario.getUsr_login());
			st.setString(5, usuario.getUsr_senha());
			
			added = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return added > 0;
	}	
		
	public int delUser(int id){
		
		int delete=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="DELETE FROM usuario WHERE usr_id=?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);		
			delete = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
		
			e.printStackTrace();
			
		}
		
		return delete;
		
	}
			
	public int updtUser(Usuario usuario){
		
		int update=0;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="UPDATE usuario SET usr_nome=?, usr_email=?, usr_telefone=?, usr_login=?, usr_senha=? WHERE usr_id=?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, usuario.getUsr_nome());
			st.setString(2, usuario.getUsr_email());
			st.setString(3, usuario.getUsr_telefone());
			st.setString(4, usuario.getUsr_login());
			st.setString(5, usuario.getUsr_senha());
			st.setInt(6, usuario.getUsr_id());
			
			update = st.executeUpdate();
			
			st.close();
			con.close();
			
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		return update;
	}		
	
}
