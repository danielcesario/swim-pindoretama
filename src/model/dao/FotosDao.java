package model.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.db.ConnectionFactory;
import model.Foto;
import model.Utilidade;



public class FotosDao {
	public Foto getFotosById(Long id){
		
		Foto foto = null;
		
		try{
			
			Connection con = ConnectionFactory.createConnection();
			String sql ="SELECT * FROM foto WHERE foto_id = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setLong(1, id);
			ResultSet rs = st.executeQuery();
			
			rs.next();
				
			foto = new Foto();
			foto.setIdFoto(rs.getLong("foto_id"));	
			foto.setTitulo(rs.getString("foto_titulo"));
			foto.setDescricao(rs.getString("foto_descricao"));
			foto.setAlbum(rs.getString("foto_album"));
			//util.setLogotipo(rs.getByte("not_texto"));
			foto.setUsr_id(rs.getInt("usr_id"));
			
			st.close();
			con.close();
			
			} catch (SQLException e){
				
				e.printStackTrace();
				
			}
		
			return foto;
			
	}
	
	
	
	
	
		public boolean add(Foto foto) throws IOException{
			int added=0;
			
			System.out.println(foto.getUsr_id());
			try{
			Connection con = ConnectionFactory.createConnection();
			String sql ="insert into foto (foto_titulo,foto_imagem,foto_descricao,foto_album,usr_id) values (?,?,?,?,?)";  
			PreparedStatement st = con.prepareStatement(sql);		
			st.setString(1,foto.getTitulo());		 
			st.setBytes(2,foto.getImagem());
			st.setString(3,foto.getDescricao());
			st.setString(4,foto.getAlbum());
			
			st.setInt(5, foto.getUsr_id());
			added = st.executeUpdate();
			st.close();
			con.close();		
			} catch (SQLException e){
				e.printStackTrace();
			}		
			return added>0;
		}
		
		public int delete(int idfoto){
			int delete=0;
			try{
			Connection con = ConnectionFactory.createConnection();
			String sql ="Delete from foto where foto_id=?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, idfoto);		
			delete = st.executeUpdate();
			st.close();
			con.close();		
			} catch (SQLException e){
				e.printStackTrace();
			}
			return delete;
		}
		
		public int deleteAlbum(String album){
			int delete=0;
			try{
			Connection con = ConnectionFactory.createConnection();
			String sql ="Delete from foto where foto_album=?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, album);		
			delete = st.executeUpdate();
			st.close();
			con.close();		
			} catch (SQLException e){
				e.printStackTrace();
			}
			return delete;
		}
		
		public List<Foto> getConsulta(){		
			List<Foto> fotos = new ArrayList<Foto>();
			try{
			Connection con = ConnectionFactory.createConnection();
			String sql ="Select * from foto";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				Foto foto = new Foto();
				foto.setIdFoto(rs.getInt("foto_id"));		
				foto.setTitulo(rs.getString("foto_titulo"));		
				foto.setDescricao(rs.getString("foto_descricao"));
				foto.setAlbum(rs.getString("foto_album"));
				fotos.add(foto);
			}		
			st.close();
			con.close();		
			} catch (SQLException e){
				e.printStackTrace();
			}		
			return fotos;
		}
		
		public List<String> getConsultaAlbum(){		
			List<String> albuns = new ArrayList<String>();
			try{
			Connection con = ConnectionFactory.createConnection();
			String sql ="Select DISTINCT foto_album from foto ORDER BY foto_album";  
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				String albumNome = rs.getString("foto_album");
				albuns.add(albumNome);
			}		
			st.close();
			con.close();		
			} catch (SQLException e){
				e.printStackTrace();
			}		
			return albuns;
		}
		
		public List<Foto> getConsultaFotosPorAlbum(String album){		
			List<Foto> fotos = new ArrayList<Foto>();
			try{
			Connection con = ConnectionFactory.createConnection();
			String sql ="Select * from foto where foto_album = ?";  
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1,album);
			ResultSet rs = st.executeQuery();
			
			while(rs.next()){
				Foto foto = new Foto();
				foto.setIdFoto(rs.getInt("foto_id"));		
				foto.setTitulo(rs.getString("foto_titulo"));		
				foto.setDescricao(rs.getString("foto_descricao"));
				foto.setAlbum(rs.getString("foto_album"));
				fotos.add(foto);
			}		
			st.close();
			con.close();		
			} catch (SQLException e){
				e.printStackTrace();
			}		
			return fotos;
		}
		
		
		public boolean update(Foto foto) throws IOException{
			int update=0;
			try{
			Connection con = ConnectionFactory.createConnection();
			String sql ="UPDATE foto SET foto_titulo=?,foto_descricao=?,foto_album=?,usr_id=? where foto_id=? ";  
			PreparedStatement st = con.prepareStatement(sql);		
			st.setString(1, foto.getTitulo());					 	
			st.setString(2, foto.getDescricao());
			st.setString(3, foto.getAlbum());			
			st.setInt(4, foto.getUsr_id());
			st.setLong(5, foto.getIdFoto());		
			update = st.executeUpdate();
			st.close();
			con.close();		
			} catch (SQLException e){
				e.printStackTrace();
			}		
			return update>0;
		}
	}
