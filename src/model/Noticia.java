package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Noticia {
	
	private int not_id;
	private String not_titulo;
	private String not_descricao;
	private String not_keywords;
	private byte[] not_imgdest;
	private String not_autor;
	private String not_texto;
	private Date not_data;
	private int user_id;
	
	public int getNot_id() {
		return not_id;
	}
	public void setNot_id(int not_id) {
		this.not_id = not_id;
	}
	public String getNot_titulo() {
		return not_titulo;
	}
	public void setNot_titulo(String not_titulo) {
		this.not_titulo = not_titulo;
	}
	public String getNot_descricao() {
		return not_descricao;
	}
	public void setNot_descricao(String not_descricao) {
		this.not_descricao = not_descricao;
	}
	public String getNot_keywords() {
		return not_keywords;
	}
	public void setNot_keywords(String not_keywords) {
		this.not_keywords = not_keywords;
	}
	public byte[] getNot_imgdest() {
		return not_imgdest;
	}
	public void setNot_imgdest(byte[] not_imgdest) {
		this.not_imgdest = not_imgdest;
	}
	public String getNot_autor() {
		return not_autor;
	}
	public void setNot_autor(String not_autor) {
		this.not_autor = not_autor;
	}
	public String getNot_texto() {
		return not_texto;
	}
	public void setNot_texto(String not_texto) {
		this.not_texto = not_texto;
	}	
	public String getNot_data() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(not_data);
	}
	
	public String getDataFormatada() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(not_data);
	}
	
	public String getHoraFormatada() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		return dateFormat.format(not_data);
	}
	
	public String getDataHoraFormatada() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
		return dateFormat.format(not_data);
	}	
	
	public void setNot_data(Date not_data) {
		this.not_data = not_data;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

}
